# Contains
# Poolman
# SplitPoolman
# Bassham

from modelbase_mini import ParameterSet


class Poolman(ParameterSet):
    def __init__(self):
        super().__init__()
        # Kinetic constants
        self.V1 = 0.34 * 8  # [mM/s], Pettersson 1988
        self.V6 = 0.2 * 8  # [mM/s], Pettersson 1988
        self.V9 = 0.04 * 8  # [mM/s], Pettersson 1988
        self.V13 = 1.0 * 8  # [mM/s], Pettersson 1988
        self.V16 = 0.35 * 8  # [mM/s], Pettersson 1988
        self.Vst = 0.04 * 8  # [mM/s], Pettersson 1988
        self.Vx = 0.25 * 8  # [mM/s], Pettersson 1988
        self.Km1 = 0.02  # [mM], Pettersson 1988
        self.Km6 = 0.03  # [mM], Pettersson 1988
        self.Km9 = 0.013  # [mM], Pettersson 1988
        self.Km131 = 0.05  # [mM], Pettersson 1988
        self.Km132 = 0.05  # [mM], Pettersson 1988
        self.Km161 = 0.014  # [mM], Pettersson 1988
        self.Km162 = 0.3  # [mM], Pettersson 1988
        self.Kmst1 = 0.08  # [mM], Pettersson 1988
        self.Kmst2 = 0.08  # [mM], Pettersson 1988
        self.Kpga = 0.25  # [mM], Pettersson 1988
        self.Kgap = 0.075  # [mM], Pettersson 1988
        self.Kdhap = 0.077  # [mM], Pettersson 1988
        self.Kpi = 0.63  # [mM], Pettersson 1988
        self.Kpxt = 0.74  # [mM], Pettersson 1988
        self.Ki11 = 0.04  # [mM], Pettersson 1988
        self.Ki12 = 0.04  # [mM], Pettersson 1988
        self.Ki13 = 0.075  # [mM], Pettersson 1988
        self.Ki14 = 0.9  # [mM], Pettersson 1988
        self.Ki15 = 0.07  # [mM], Pettersson 1988
        self.Ki61 = 0.7  # [mM], Pettersson 1988
        self.Ki62 = 12.0  # [mM], Pettersson 1988
        self.Ki9 = 12.0  # [mM], Pettersson 1988
        self.Ki131 = 2.0  # [mM], Pettersson 1988
        self.Ki132 = 0.7  # [mM], Pettersson 1988
        self.Ki133 = 4.0  # [mM], Pettersson 1988
        self.Ki134 = 2.5  # [mM], Pettersson 1988
        self.Ki135 = 0.4  # [mM], Pettersson 1988
        self.Kist = 10.0  # [mM], Pettersson 1988
        self.Kast1 = 0.1  # [mM], Pettersson 1988
        self.Kast2 = 0.02  # [mM], Pettersson 1988
        self.Kast3 = 0.02  # [mM], Pettersson 1988

        self.kRE = 8e8  # Rapid Equilibrium speed

        # Equilibrium constants
        self.q2 = 3.1 * (10.0 ** (-4.0))  # [], Pettersson 1988
        self.q3 = 1.6 * (10.0**7.0)  # [], Pettersson 1988
        self.q4 = 22.0  # [], Pettersson 1988
        self.q5 = 7.1  # [1/mM], Pettersson 1988
        self.q7 = 0.084  # [], Pettersson 1988
        self.q8 = 13.0  # [1/mM], Pettersson 1988
        self.q10 = 0.85  # [], Pettersson 1988
        self.q11 = 0.4  # [], Pettersson 1988
        self.q12 = 0.67  # [], Pettersson 1988
        self.q14 = 2.3  # [], Pettersson 1988
        self.q15 = 0.058  # [], Pettersson 1988

        # Pools
        self.CO2 = 0.2  # [mM], Pettersson 1988
        self.Cp = 15.0  # [mM], Pettersson 1988
        self.Ca = 0.5  # [mM], Pettersson 1988
        self.CN = 0.5  # [mM], Pettersson 1988
        self.Pext = 0.5  # [mM], Pettersson 1988

        # pH
        self.pHmedium = 7.6
        self.pHstroma = 7.9
        self.protonsStroma = 10 ** (-7.9) * 1000.0  # [mM]

        # NADP(H) pools for basic poolman model
        self.NADPH = 0.21
        self.NADP = 0.29


class SplitPoolman(Poolman):
    """Poolman parameters split to allow
    label model usage"""

    def __init__(self):
        super().__init__()
        # Remove old parameters
        self.remove_parameter("kRE")
        # Add new parameters
        self.k2f = 8e8
        self.k2r = 8e8
        self.k3f = 8e8
        self.k3r = 8e8
        self.k4f = 8e8
        self.k4r = 8e8
        self.k5f = 8e8
        self.k5r = 8e8
        self.k7f = 8e8
        self.k7r = 8e8
        self.k8f = 8e8
        self.k8r = 8e8
        self.k10f = 8e8
        self.k10r = 8e8
        self.k11f = 8e8
        self.k11r = 8e8
        self.k12f = 8e8
        self.k12r = 8e8
        self.k14f = 8e8
        self.k14r = 8e8
        self.k15f = 8e8
        self.k15r = 8e8


class Bassham(SplitPoolman):
    def __init__(self):
        super().__init__()
        self.remove_parameter("q2")
        self.remove_parameter("q3")
        self.remove_parameter("q4")
        self.remove_parameter("q5")
        self.remove_parameter("q7")
        self.remove_parameter("q8")
        self.remove_parameter("q10")
        self.remove_parameter("q11")
        self.remove_parameter("q12")
        self.remove_parameter("q14")
        self.remove_parameter("q15")
        self.k2f = 9.201751269188145
        self.k2r = 32582.211592466298
        self.k3f = 544637974.9812177
        self.k3r = 6.770073507981515e-05
        self.k4f = 71.12668763739163
        self.k4r = 2.536954631987327
        self.k5f = 26.59188391761439
        self.k5r = 2.857113867555865
        self.k7f = 19.397240851045034
        self.k7r = 171.32156056378304
        self.k8f = 70.05167257677905
        self.k8r = 5.610915648763027
        self.k10f = 35.747464921294416
        self.k10r = 37.35248352641369
        self.k11f = 48.64771237135582
        self.k11r = 116.41666392881288
        self.k12f = 157.52592237170072
        self.k12r = 232.8333228289382
        self.k14f = 0.049555583505605835
        self.k14r = 0.021677578735797107
        self.k15f = 0.014301375975283213
        self.k15r = 1.3767755716272655e-10
        self.V1 = 1.1470205806062528
        self.V6 = 0.4549249003859781
        self.V9 = 0.30125280912820146
        self.V13 = 24.755929769008766
        self.V16 = 2.6270790430755477
        self.Vst = 0.1223590855373052
        self.Vx_PGA = 1.1066699104291686
        self.Vx_GAP = 0.8479624058609916
        self.Vx_DHAP = 0.9327585789768257

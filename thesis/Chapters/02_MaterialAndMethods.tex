\chapter{Materials and Methods}\label{sec:materials-and-methods}

\section{Theoretical background}\label{sec:theoretical-background}

\subsection{Ordinary differential equations}

The models build in this thesis are based on ordinary differential equations (ODEs). An ordinary differential equation is a mathematical equation containing one or more functions of one, and only one, independent variable and its derivatives. Thus ODEs describe the rate of change of the respective independent variable. A simple example of an ODE with the independent variable $x$ is

\begin{equation}
	\frac{dx}{dt} = \dot{x} = f(x)
\end{equation}

To model biochemical processes, ODEs can be used to couple the concentration of a compound and its rate of change,

\begin{equation}
	\dot{[S]} = -k \cdot [S]
\end{equation}

whereas the rate of change, the kinetic rate equation, can be adapted in order to mirror the reaction mechanism.

\subsection{Kinetic rate equations}
% Mass action
The kinetic rate equations used in this work are based on mass action, for reactions close to equilibrium, and Michaelis-Menten kinetics. For example the first order mass-action reaction

\begin{equation}
	A \ce{<=>} B
\end{equation}

yields the ODEs

\begin{equation}
\begin{split}
	\frac{dA}{dt} &= - k_f A + k_r B\\
	\frac{dB}{dt} &= - k_r B + k_f A\\
\end{split}
\end{equation}

For this reaction the mass action ratio is defined as

\begin{equation}\label{eq:q}
Q = \frac{B^B}{A^a}
\end{equation}

which at chemical equilibrium is equal to the equilibrium constant

\begin{equation}
Q = K_{eq} = \frac{k^+}{k^-}
\end{equation}

The steady state of such a reaction is defined as 

\begin{equation}
	\frac{d\vec{X}(t)}{dt} = 
	\begin{bmatrix} 
		\frac{dA}{dt} \\ 
		\frac{dB}{dt} 
	\end{bmatrix} = 0	
\end{equation}


% Michaelis Menten
The reactions further displaced from equilibrium are modeled using Michaelis Menten kinetics. Consider the reaction

\begin{equation}
E + S \ce{<=>[k_{on}][k_{off}]} ES  \ce{->[k_{cat}] E + P}
\end{equation}

With the quasi steady-state approximation (QSSA), 

\begin{equation}
\frac{d[ES]}{dt} = 0
\end{equation}

a metabolite concentration far exceeding the enzyme concentration

\begin{equation}
[S]^{free} \approx [S]^{total} 
\end{equation}

and a uniform distribution of E and S, it is possible to show

\begin{equation}
\begin{split}
	\dot{[ES]} &= k^{on} [E] [S]  - k^{off} [ES] - k^{cat} [ES] \\
	0 &= k^{on} [E] [S]  - k^{off} [ES] - k^{cat} [ES] \\
	[ES] &= \frac{k^{on} [E] [S]}{k^{off}+k^{cat}} \\
	[ES] &= \frac{[E] [S]}{K_m} \\
	[ES] &= \frac{([E^{Total}] - [ES]) [S]}{K_m} \\
	[ES] \left( 1 + \frac{[S]}{K_M} \right) &= \frac{[E^{Total}] [S]}{K_m} \\
	[ES] &= \frac{\frac{[E^{Total}] [S]}{K_M}}{\left( 1 + \frac{[S]}{K_m} \right) } \\
	[ES] &= \frac{[E^{Total}][S]}{K_m + [S]}
\end{split}
\end{equation}

which means that the change of product concentration can be expressed as

\begin{equation}
\begin{split}
	\dot{[P]} &= k^{cat} [ES] \\
	&= \frac{k^{cat} [E^{Total}][S]}{K_M + [S]} \\
	&= \frac{V_{Max} [S]}{K_M + [S]}
\end{split}
\end{equation}

\section{modelbase}\label{sec:modelbase}
\modelbase \ is a free Python package for building and analysing dynamic mathematical models. Although it was designed for the simulation of metabolic systems and labeling patterns in such systems, it can be used for virtually any deterministic chemical process. The package provides easy standardized model construction methods by building the model with rates and stoichiometries. Since the ODE system is assembled internally, analysis routines can be standardized as well. This increases the reproducibility, by minimizing user error. \cite{Ebenhoeh2018b}

\subsection{Model creation with modelbase}
To understand the need for creating the \mbm \ fork, it is necessary to understand how the basic model creation in \modelbase \ works. 
1. A \mintinline{python3}{modelbase.Model} class is instantiated with a parameter dictionary, which is then internally stored as a \mintinline{python3}{ParameterSet} class, providing the more convenient \mintinline{python3}{.dot} notation to access its attributes. 
2. A list of compounds is added to the model with \mintinline{python3}{set_cpds(compound_list)}, which is stored as a list and at the same time a dictionary of the enumerated compounds is created internally using \mintinline{python3}{updateCpdIds()}. This dictionary is later used to index the input vector in order to pass the right function arguments. Additional compounds can later be added using \mintinline{python3}{add_cpd()} and \mintinline{python3}{add_cpds()}, but they cannot be removed from the model.
3. A rate is added with \mintinline{python3}{set_rate(rate_name, rate_function, *args)}, with \mintinline{python3}{*args} being the rate function arguments, which are equal to the compounds. To do this first the integers of the enumerated compound dictionary (step 2) are retrieved and then the rate function is wrapped inside a wrapper function \mintinline{python3}{v}, which internally stores the compound ids. Rates cannot be removed from the model at a later stage.
4. The rate stoichiometriy is set with \mintinline{python3}{set_stoichiometry(rate_name, stoichiometry_dict)}, which is stored as a dictionary.

To integrate the model, a dictionary of the rate functions is created and then the right hand side is created by multiplying the rate with its stoichiometry for each compound.

\section{modelbase mini}\label{sec:modelbase-mini}
\mbm \ is my fork of the modelbase package, which is aimed at providing a more consistent user interaction and internal project structure, while at the same time providing full modularity. At the time of the fork, the \modelbase \ project had one known major problem and a few inconsistencies regarding the user interaction. The main problem arose from the way rates are constructed (see text above). Since the compound ids are stored inside the wrapper functions, removing compounds leads to wrong indices for all reactions. The same happens if the system contains algebraic modules, as the derived compounds are indexed after after all other compounds are indexed. So if a compound is added, after rates that depend on algebraic modules have been added, the indices are again pointing to the wrong compound.

In \mbm \ this has been avoided by not wrapping the functions, but by instead zipping the compound vector with the compound names and calling each rate function with its respective dictionary values. To enable this, the function \mintinline{python3}{args} are stored in a dictionary as well.

To achieve full modularity, each compound, parameter, rate and algebraic module can, at any time point, be added and removed. To avoid confusion of the user, internal objects are blocked from view by making them "private" and \mintinline{python3}{get_x()} functions were added to access them. This improves the inconsistent handling of internal objects in \modelbase. \mbm \ introduces convenience plotting routines, especially for label models to ease early usage of the package. \\
To get an overview of the functions of the package, see the UML diagram \ref{fig:uml-mbm}.

\chapter{Results}\label{sec:results}

In order to study the label distribution of the CBB cycle \textit{in silico}, a suitable model had to be created, see section \ref{sec:model-development}. In this work, the Poolman CBB model \cite{Poolman2000} was chosen as a base model. The model is a variant of the Pettersson and Ryde-Pettersson \cite{Pettersson1988} model, where the strict rapid equilibrium assumption is relaxed and fast reactions are modeled by simple mass action kinetics, with rapid rate constant $k_{re} = 5\cdot 10^8$ for all reactions, regardless of the molecularity of the reaction, see section \ref{sec:poolman-variant}. The concentrations of NADPH, NAD$^+$, CO$_2$ and H$^+$ are considered constant, leaving the 13 carbohydrate cycle intermediates, ATP, ADP and inorganic phosphate as dynamic variables. The model further incorporates a simplified starch production using glucose 6-phosphate and glucose-1-phosphate and a simple ATP recovery reaction.
All the models were created using \mbm, a fork of the \modelbase \ project, that was created during this work, see chapter \ref{sec:modelbase-mini}.

%----------------------------------------------------------------------------------------
%	Poolman model validation
%----------------------------------------------------------------------------------------
\section{Validation of Poolman model implementation}\label{sec:validation-of-poolman-model-implementation}
In order to validate the correct implementation of the Poolman model, the metabolite steady-state concentrations, fig. \ref{fig:steady-state-poolman}, and the steady state concentrations of the metabolites depending on the extra-stromal phosphate concentration, fig. \ref{fig:poolman_stability}, were simulated. This reproduced original work by Pettersson and Ryde-Pettersson \cite{Pettersson1988}. 

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/steady-state-poolman.pdf}
	\caption[Poolman steady state metabolite concentrations]{Metabolite concentrations in mM over time in seconds.}
	\label{fig:steady-state-poolman}
\end{figure}

As figure \ref{fig:poolman_stability} shows, the system is not stable any more for $[P_{ext}]>1.5$, a feature not discussed in the Poolman paper \cite{Poolman2000}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/stability-poolman.pdf}
	\caption[Poolman steady state concentrations depending on extra-stromal phosphate]{Steady state concentrations of the metabolites depending on the extra-stromal phosphate concentration. The system is not stable any more for \([P_{ext}]>1.5\)}
	\label{fig:poolman_stability}
\end{figure}


%----------------------------------------------------------------------------------------
%	Label model
%----------------------------------------------------------------------------------------

\section{Label model}\label{sec:label-model}
The label model was build by using carbon transition maps to create all possible reactions and labeled compounds, see section \ref{sec:labeled-cbb-model}. In total it contains 689 compounds and 4233 reactions. Figure \ref{fig:poolman-fbp-label-distribution} shows the distribution of labeled carbon positions of FBP over a time span of 10 arbitrary time units. The label pair C3 and C4 is labeled the most, the C1 and C6 pair comes up second and the C2 and C5 pair is labeled the least. The label positions C1 and C6, C2 and C5 and C3 and C4 are each overlapping.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/label-fbp-poolman.pdf}
	\caption[Poolman FBP label distribution]{Label distribution relative to total FBP concentration over time in arbitrary units.}
	\label{fig:poolman-fbp-label-distribution}
\end{figure}

\subsection{New rate parameters}
A hypothesis was formed that the labeling pattern observed in figure \ref{fig:poolman-fbp-label-distribution} was caused by the rapid reaction constant introduced by Poolman \cite{Poolman2001}. For example, the $\frac{C4}{C3}$ asymmetry is thought to be caused by the different GAP and DHAP pool sizes and thus the different level of percentual label concentration, an effect that is void by by rapid equilibrium of the percentual label concentration due to the rapid reaction constant. This is further discussed in section \ref{sec:rate-parameters}. 
Therefore, a new set of parameters was calculated using concentrations and free energies measured by Bassham \cite{Bassham1969}, see section  \ref{sec:parameter-fitting}. \\
The new mass action rate parameters were calculated using equations \ref{eq:ma_rate_params_forward} and \ref{eq:ma_rate_params_forward}, for the parameter values see table \ref{table:new_params}. The reaction rates were adjusted to standard mass action kinetic form, eq. \ref{eq:standard-mass-action}, and new $V_{Max}$ values were calculated in order to reflect the changes of the Bassham concentrations on the regulatory terms. In the following the model with the measured Bassham concentrations and derived parameters will be refered to as the "Bassham" model. \\
The Bassham model yields a stable steady state, as shown in figure \ref{fig:bassham-steady-state}. 

% New steady state
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/steady-state-bassham.pdf}
	\caption[Bassham steady state metabolite concentrations]{Metabolite concentrations in mM over time in seconds.}
	\label{fig:bassham-steady-state}
\end{figure}

However the model shows changed behaviour and reduced stability regarding extra-stromal phosphate concentration in comparison to the Poolman implementation, see fig. \ref{fig:poolman_stability}, and the original Pettersson model \cite{Pettersson1988}. As discussed in \ref{sec:rate-parameters}, they still were used for all following simulations.

% New stability
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/stability-bassham.pdf}
	\caption[Bassham steady state concentrations depending on extra-stromal phosphate]{Steady state concentrations of the metabolites depending on the extra-stromal phosphate concentration. The system shows reduced stability regarding extra-stromal phosphate concentration in comparison to the Poolman implementation, as concentrations slightly above $[P_{ext}] = 0.5$ cause the system to collapse.}
	\label{fig:stability-bassham}
\end{figure}

\subsection{Bassham label model}
Figure \ref{fig:bassham-all-compounds} shows the label distribution of all compounds containing carbon atoms, relative to the total compound concentration.
All possible carbon positions are labeled, to varying extent. The strongest relative labeling can be observed in the C1 positions of PGA, BPGA and GAP respectively, with the C3 position of DHAP coming in second. The second and third carbons (second and first for DHAP) are labeled more slowly, the third position being labeled more than the second.
FBP, F6P, G6P and G1P show an asymmetry in the C4 and C3 positions, as well as in the C2 and C5 and C1 and C6 positions. For all those four compounds the $\frac{C4}{C3}$ fraction is greater than one. For FBP, the $\frac{C2}{C5}$ and $\frac{C1}{C6}$ fractions is smaller than one, while they are larger than one for F6P, G6P and G1P. This effect is more visible in figure \ref{fig:bassham-asymmetries-over-time}.
The maximal relative level of G6P and G1P labeling is $\frac{1}{6}$ and $\frac{1}{60}$ that of F6P respectively. The relative level of G6P and G1P labeling is lower than that of all other compounds. As discussed in section \ref{sec:labeling}, F6P was chosen as the reference point for further analysis, as the labeling asymmetry pattern of F6P, G6P and G1P is qualitatively equal while being skewed by time. 
E4P is labeled mainly in the first and second position, the second position being labeled slightly more. 
SBP and S7P are labeled mainly in C3, C4 and C5. The C3 position is labeled the most, while the C5 position is labeled more than the C4 position. The $\frac{C5}{C4}$ asymmetry is more pronounced in S7P than in SBP.
The label pattern of X5P, R5P, Ru5P and RuBP is nearly equal, with C1 being the main label, followed by C1 and C2. The $\frac{C1}{C2}$ asymmetry is slightly more pronounced in R5P than in the other compounds.


% New labels (all compounds)
\begin{figure}[H]
	\centering
	\includegraphics[height=0.8\textheight]{Plots/label-all-bassham.pdf}
	\caption[Bassham label distribution]{Label distribution in all compounds containing carbon atoms, relative to the compounds total concentration. All possible carbon positions are labeled, to varying extend.}
	\label{fig:bassham-all-compounds}
\end{figure}

\newpage

Figure \ref{fig:bassham-asymmetries-over-time} shows the $\frac{C4}{C3}$, $\frac{C2}{C5}$ and $\frac{C1}{C6}$ labeling asymmetries in FBP, F6P, G6P and G1P against the relative amount of labeled against total compound concentration. In FBP, the $\frac{C4}{C3}$ asymmetry is $> 1$, while the other two asymmetries are $< 1$. In F6P, G6P and G1P all observed label asymmetries are $> 1$ at low relative label concentration. At the point of maximal relative label concentration no asymmetries between $\frac{C4}{C3}$, $\frac{C2}{C5}$ and $\frac{C1}{C6}$ are observable. The magnitude of the $\frac{C4}{C3}$ fraction is $> 1$ but $< 10$ for all observed compounds. The $\frac{C2}{C5}$ and $\frac{C1}{C6}$ asymmetries in F6P are 4 orders of magnitude larger than those in G1P and two orders of magnitude larger than those of G6P.

% New labels (over time)
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/label-asymmetries-over-time-bassham.pdf}
	\caption[FBP label asymmetries]{$\frac{C4}{C3}$, $\frac{C2}{C5}$ and $\frac{C1}{C6}$ labeling asymmetries in FBP, F6P, G6P and G1P depending on relative label concentration. In FBP, the $\frac{C4}{C3}$ asymmetry is $> 1$, while the other two asymmetries are $< 1$. In F6P, G6P and G1P all observed label asymmetries are $> 1$. At the point of maximal relative label concentration no asymmetries between $\frac{C4}{C3}$, $\frac{C2}{C5}$ and $\frac{C1}{C6}$ are observable.}
	\label{fig:bassham-asymmetries-over-time}
\end{figure}

Animations showing the relative label concentrations of all carbons and all compounds were created. They can be obtained from the project \href{https://gitlab.com/marvin.vanaalst/labeled-calvin-cycle}{gitlab repository}.


%----------------------------------------------------------------------------------------
%	Actual analysis
%----------------------------------------------------------------------------------------
\section{Label asymmetries}
\subsection{Enzyme expression}
To observe the effect of enzyme concentration and thus the resulting changed compound and label concentrations on the labeling pattern, a scan over all reactions was performed. In the case of the reactions described by mass action kinetics, the reaction term was multiplied by a factor $i$, resulting in the canonical expression $v = i \cdot \left(k^+ \prod_{i}S_i - k^- \prod_{j} P_j\right)$. In the case of reactions described by Michaelis-Menten kinetics, the $V_{Max}$ of the reaction was multiplied by a factor $i$, resulting in the canonical expression $v = \frac{V_{Max} \cdot i \cdot [S]}{[S] + K_m \cdot R(S)}$.
Figure \ref{fig:bassham-enzyme-label-distribution-tpi} shows the $\frac{C4}{C3}$ label asymmetriy depending on the level of labels relative to the compound concentration and enzyme concentration factor $i$ on the reaction $v_4$, which corresponds to the enzyme Triosephosphate isomerase (TPI). TPI catalyzes the isomerization reaction of GAP to DHAP. The $\frac{C4}{C3}$ increases for decreasing enzyme concentration and decreases for increasing enzyme concentration, thus showing an anti proportional effect.

% C4/C3
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.8\textwidth]{Plots/enzyme-label-distribution-Bassham-TPI-(v4)-F6P-c4c3.pdf}
	\caption[TPI activity screen]{$\frac{C4}{C3}$ label asymmetries depending on enzyme concentration factor and the level of labels relative to the compound concentration.}
	\label{fig:bassham-enzyme-label-distribution-tpi}
\end{figure}


Figures \ref{fig:c4c3-heatmap} - \ref{fig:c1c6-heatmap} give an condensed view of the effect of the enzyme concentration variation at the time point 0.1 [au], and fixed enzyme concentration factors between 0.1 and 10. However, all 3D plots showing the the effect of both enzyme concentration and relative label concentration can be accessed in the accompanying  \href{https://gitlab.com/marvin.vanaalst/labeled-calvin-cycle}{gitlab repository}. \\ 
In figure \ref{fig:c4c3-heatmap}, the effect of systematic enzyme perturbations on the $\frac{C4}{C3}$ label asymmetry on F6P is shown. There is an anti proportional effect of enzyme activity to label asymmetry for the TPI activity, as shown before in figure \ref{fig:bassham-enzyme-label-distribution-tpi}, and a less pronounced anti proportional effect of Aldolase and SBPase. There also is a proportional effect of ATP Synthase and Transketolase.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/c4c3-heatmap.pdf}
	\caption[Enzyme activity heatmap $\frac{C4}{C3}$]{Heatmap showing the influence of enzyme perturbation on the F6P $\frac{C4}{C3}$ label asymmetry. The x-axis shows the factor of enzyme change. For white fields the system did not reach a steady state. The calculated values are linearly interpolated with the respective enzyme perturbation factor.}
	\label{fig:c4c3-heatmap}
\end{figure}

Figures \ref{fig:c2c5-heatmap} and \ref{fig:c1c6-heatmap} show the effect of systematic enzyme concentration variation on the $\frac{C2}{C5}$ and $\frac{C1}{C6}$ label asymmetry on F6P. In contrast to figure \ref{fig:c4c3-heatmap}, the results are shown $\log_{10}$ transformed. For both asymmetries an anti proportional effect of TPI, SBPase, Transketolase, ATPsynthase and PRK can be observed. However a combination of RPE with Transketolase shows a large proportional increase, as do Aldolase and the combination of Transketolase and Aldolase.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/c2c5-heatmap.pdf}
	\caption[Enzyme activity heatmap $\frac{C2}{C5}$]{Heatmap showing the influence of enzyme perturbation on the F6P $\frac{C4}{C3}$ label asymmetry. The x-axis shows the factor of enzyme change. For white fields the system did not reach a steady state. The calculated values are linearly interpolated with the respective enzyme perturbation factor. The y-values and colormap are shown $\log_{10}$ transformed.}
	\label{fig:c2c5-heatmap}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/c1c6-heatmap.pdf}
	\caption[Enzyme activity heatmap $\frac{C1}{C6}$]{Heatmap showing the influence of enzyme perturbation on the F6P $\frac{C4}{C3}$ label asymmetry. The x-axis shows the factor of enzyme change. For white fields the system did not reach a steady state. The calculated values are linearly interpolated with the respective enzyme perturbation factor. The y-values and colormap are shown $\log_{10}$ transformed.}
	\label{fig:c1c6-heatmap}
\end{figure}

\subsection{Models without $\ce{^{14}C}$ influx}\label{sec:models-without-labeled-co2}
In the simulations of this section, the constant influx of labeled $\ce{^{14}C}$ was changed to unlabeled $\ce{^{12}C}$, enabling precise tracking of the path a labeled carbon can take. In each simulation the model was thus initialized with compounds fully labeled at the given positions. All plots show the label concentrations relative to the respective compound concentrations of FBP and F6P.

\subsubsection{C1/C6 and C2/C5}
In the simulation for figure \ref{fig:no-co2-x5p-12} X5P was initialized fully labeled in positions 1 and 2. The maximal total label concentration of F6P is higher than the one of FBP. For FBP the concentrations for C5 and C6 are higher than C1 and C2 in the first two time units. For F6P the opposite is the case. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/no-co2-x5p-12.pdf}
	\caption[No $\ce{^{14}C}$ influx - $X5P^{12}$]{Absolute label concentration in FBP and F6P against time in arbitrary units. The model was initialized without constant label influx, but with X5P fully labeled in positions 1 and 2. C1 and C5 are plotted with thicker lines, as they completely overlap with C2 and C6 respectively.}
	\label{fig:no-co2-x5p-12}
\end{figure}

Figure \ref{fig:no-co2-e4p-34} shows the same experiment, but with E4P labeled in positions C2 and C3. For FBP the concentrations for C5 and C6 are again higher than C1 and C2, but in contrast to the previous experiment this now also holds for F6P. Another observation is that for both experiments the label concentrations for the position pairs C1 and C2, as well as C5 and C6 are equal, in contrast to the simulations with the constant labeled $\ce{CO_2}$ influx.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/no-co2-e4p-34.pdf}
	\caption[No $\ce{^{14}C}$ influx - $E4P^{23}$]{Absolute label concentration in FBP and F6P against time in arbitrary units. The model was initialized without constant label influx, but with E4P fully labeled in positions 3 and 4. C1 and C5 are plotted with thicker lines, as they completely overlap with C2 and C6 respectively.}
	\label{fig:no-co2-e4p-34}
\end{figure}

\subsubsection{C1/C2 and C5/C6}
Figure \ref{fig:no-co2-rubp-12} shows the label distribution of FBP and F6P with RUBP equally labeled in positions 1 and 2, while in figure \ref{fig:no-co2-rubp-12-asymmetrically} the same is plotted for an asymmetrical label distribution in RUBP ($\frac{2}{3}$ of the RUBP molecules are labeled in position 1, $\frac{1}{3}$ are labeled in position 2). 

In figure \ref{fig:no-co2-rubp-12} the concentrations of the pairs C5 and C6 and C1 and C2 each are equal. The C5 and C6 pair is labeled more in the first two arbitratry time units for both FBP and F6P.

 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Plots/no-co2-rubp-12}
	\caption[No $\ce{^{14}C}$ influx - $\ce{RUBP^{12}}$]{Absolute label concentration of FBP and F6P, after initialization with $\ce{RUBP^{12}}$ labeled equally in both positions. There is no constant $\ce{^{14}C}$ influx.}
	\label{fig:no-co2-rubp-12}
\end{figure}

In figure \ref{fig:no-co2-rubp-12-asymmetrically} on the other hand the concentrations of these pairs differ internally. In the range of the first two arbitrary time units, for both FBP and F6P C6 is labeled more than C1 and C5 is labeled more than C2. Both C6 and C1 are labeled more than C5 or C2.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{Plots/no-co2-rubp-12-asymmetrically}
	\caption[No $\ce{^{14}C}$ influx - asymmetric $\ce{RUBP^{12}}$]{Absolute label concentration of FBP and F6P, after initialization with $\ce{RUBP^{12}}$ labeled $\frac{2}{3}$ in position 1 and $\frac{1}{3}$ in position 2. There is no constant $\ce{^{14}C}$ influx.}
	\label{fig:no-co2-rubp-12-asymmetrically}
\end{figure}



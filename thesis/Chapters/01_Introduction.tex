\chapter{Introduction}\label{sec:introduction}

\section{Photosynthesis}\label{sec:photosynthesis}
All life on earth needs energy in order to create ordered structures. For the majority of organisms this energy comes, directly or indirectly, from the sun's light energy. Photosynthesis is the only biochemical process capable of harvesting and storing that energy and is shared by plants, algae and cyanobacteria \cite{BLACK1963}. Since in most cases oxygen is produced as a byproduct, photosynthesis is the foundation for modern life on this planet. Thus it is one of the most important biochemical processes \cite{Hartman1998}. \\
Photosynthesis consists of two major reaction cycles: the light-dependent reactions and the light-independent reactions. Both happen inside the chloroplast of the photoautotrophic organism. Within the chloroplast are disk-like structures called thylakoids, which are surrounded by the chloroplast stroma. The light-dependent reactions occur in the thylakoid lumen, the light-independent reactions in the stroma.

\subsection{Light-dependent reactions}\label{sec:light-dependent-reactions}
In the light-dependent reactions, the photon energy is converted into chemical energy in the form of ATP and NADPH.
In most photosynthetic organisms, thylakoids contain pairs of photosystems, called photosystem I (PSI) and photosystem II (PSII). In each photosystem a network of accessory pigment and chlorophyll molecules, called light-harvesting complex (LHC), absorbs photons. In these LHCs, the absorbed photon energy excites electrons. The photosystems channel the excitation energy to a reaction center chlorophyll, which passes the electrons to the thylakoid membrane. \\
In PSII the channeled light energy catalyses photolysis, the oxidation of a water molecule to form molecular oxygen and protons in the thylakoid lumen and free electrons. The electrons are transferred through the thylakoid membrane by plastoquinone (PQ) to the cytochrome b6f complex (Cytb6f). The Cytb6f complex catalyses the translocation of stromal protons to the thylakoid lumen while transferring the electrons to PSI via plastocyanin or cytochrome c6. 
The low-energy electrons are re-energized in PSI and passed through an electron transport chain, where they are used to reduce NADP$^+$ to NADPH via ferredoxin-NADP reductase. \\
The concentration gradient resulting from the proton translocation and photolysis powers ATP synthase, which catalyses the phosphorylation of ADP. 

\subsection{Light-independent reactions}\label{sec:light-independent-reactions}
The light-independent reactions, also termed Calvin-Benson-Bassham (CBB) cycle, store the energy obtained in the light-dependent reactions in carbohydrates. The complete fixation can be expressed by the following formula.

\begin{equation}
    \mathrm{CO_2 + H_2O -> (CH_2O)_n + O_2}
\end{equation}

Despite this simple looking equation, the CBB cycle is a complex metabolic network. It can be separated into three main steps:

\begin{enumerate}
    \item Fixation of carbon dioxide
    \item Reduction of substrates
    \item Regeneration of RuBP
\end{enumerate}

\begin{figure}[htb]
	\includegraphics[width=\textwidth]{Images/cbb-cycle.pdf}
	\caption{The carbon containing compounds of the CBB cycle.}
	\label{fig:cbb_cycle}
\end{figure}

The fixation of carbon dioxide is done by RuBisCO, probably the most abundant protein on this planet \cite{Ellis1979}. RuBisCO attaches $\ce{CO_2}$ to RuBP, resulting in a six carbon molecule that splits into two PGA. \\
The PGA molecules are reduced to GAP by phosphorylating the PGA molecules, each requiring the dephosphorylation of an ATP molecule. The two resulting BPGA molecules are reduced by glyceraldehyde 3-phosphate dehydrogenase into two GAP, each oxidizing an NADPH. \\
Over a series of nine steps RuBP is regenerated, which consumes another ATP.
For every three turns of the cycle, five molecules of GAP are used to reform 3 RuBP. The remaining GAP is used to make glucose, fatty acids or glycerol. It takes two molecules of GAP to make one glucose molecule. Thus, the CBB cycle has to run 6 time to produce one molecule of glucose, which can be added to fructose to form sucrose or be used as a starting molecule for the synthesis of starch and cellulose.

\section{Carbon labeling}
A carbon label is a form of isotopic labeling, in which a $\ce{^{12}C}$ atom has been replaced by e.g. a $\ce{^{11}C}$ or $\ce{^{14}C}$ atom, in the case of radioactive, or  $\ce{^{13}C}$ in the case of stable isotope labeling. To distinguish between compounds with multiple carbon atoms, the carbons are numbered. For aldoses, the carbonyl carbon is given the number one and following carbons down the chain are numbered in ascending order (top to bottom in Fischer projection). For ketoses, the topmost carbon at the carbon chain that is shorter (counting from the ketone) is given the number one. The following numbers are given as with aldoses. \textbf{In this work the notation that e.g. a PGA molecule labeled in the first position will be written as PGA$^1$ is used. \\}
The CBB cycle contains at least six\footnotemark \  reactions that change the order in which substrates and products are numbered: Ribulose-1,5-bisphosphate carboxylase/oxygenase (RuBisCO), Triose phosphate isomerae (TPI), Aldolase (ALD, 2 reactions) and Transketolase (TK, 2 reactions). In this work, only those standard reactions are used. \\
RuBisCO catalyzes the carboxylation and oxygenation of RuBP. The oxygenase reaction of RuBisCO however is not studied in this work. During the carboxylation RuBP is brought into its endiol form onto which $\ce{CO_2}$ is added. The resulting product 2-carboxy-3-keto-D-arabinit-1,5-bisphosphate, which fragments into two PGA molecules after agglomeration of water. Carbons one to three of RuBP form one of the PGA molecules, but positions one and three are swapped. Carbons four and five of RuBP form carbons two and three of PGA, while the carbon of $\ce{CO_2}$ is inserted into the first position. 
Triose phosphate isomerase catalyses the isomerisation reaction of the aldose GAP to the ketose DHAP, during which the one and three positions are switched.
Aldolase catalyzes reversible reactions creating or cleaving an aldol. In the calvin cycle the reaction of DHAP and GAP to FBP and DHAP and E4P to SBP are known. In both cases the ketose DHAP is cleaved and transfered to the aldose (GAP or E4P), forming the aldols FBP and SBP respectively. Transketolase takes an aldose acceptor and a ketose donor as substrates and then catalyses the transfer of the two topmost carbons of the ketose to the aldose. \\

\footnotetext{While the transketolase reactions GAP + F6P $\ce{<=>}$ E4P + X5P and S7P + GAP $\ce{<=>}$ R5P + X5P are known to occur, due to the broad substrate specificity there is no reason that the enzyme might not also catalyse the non-standard conversion of E4P + S7P $\ce{<=>}$ R5P + F6P, neutral conversions like GAP + X5P $\ce{<=>}$ GAP + X5P or reactions including octoses \cite{Ebenhoh2018}.}


\section{The photosynthetic Gibbs effect}\label{sec:the-photosynthetic-gibbs-effect}
In 1957 Otto Kandler and Martin Gibbs measured the labeling pattern of $\ce{^{14}C}$ labeled glucose. In three experiments on \textit{Chlorella} they observed an asymmetric labeled distribution during photosynthesis \cite{Gibbs1957}. As shown in table \ref{table:labeldistr}, this label distribution equilibrates for long time spans. However, in the beginning of all their measurements, the fourth carbon is labeled more than the third one, the second more than the fifth and the first more than the sixth carbon. \\


\begin{table}
	\centering
	\begin{tabular}{ l c c c}
		Time & \(\frac{C4}{C3}\) & \(\frac{C2}{C5}\) & \(\frac{C1}{C6}\) \\
		10 s & 1.34 & 2.7 & 1.56\\
		60 s & 1.36 & 2.74 & 2.52 \\
		45 min & 1.04 & 1.01 & 0.97 \\
	\end{tabular} \\
	\caption[Measured label asymmetries]{Measured label asymmetries in \textit{Chlorella} \cite{Gibbs1957}.}
	\label{table:labeldistr}
\end{table}


Bassham proposed in 1964 that the asymmetry in the 3 and 4 positions can be explained by different pool sizes of GAP and DHAP \cite{Bassham1964}. The reversible reaction of TPI is close to equilibrium, but the DHAP pool is around 20 times larger. Therefore, the fraction of labeled DHAP molecules increases slower than the fraction of labeled GAP molecules. Since the C1 atom of GAP leads to the C4 atom of FBP and the C3 atom of DHAP to the C3 atom of FBP, this difference in the fraction of labeled molecules leads to the $\frac{C4}{C3}$ asymmetry. Bassham further states that the $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries can be "readily explained in terms of the reversibility of the transketolase reaction", and that the effect will depend "to a large extent on pool sizes and the rate of the net forward reaction as compared with the rate of the reverse reactions" \cite{Bassham1964}. However, up to this day no quantitative explanation of both these proposed explanations has been given.
\chapter{Model development}\label{sec:model-development}
The Poolman variant \cite{Poolman2000} of the Pettersson CBB model \cite{Pettersson1988} was used as a basis for the labeled model.

\section{Pettersson CBB model}\label{sec:pettersson-cbb-model}
The Pettersson model describes the calvin cycle using 16 dynamic variables (ATP, PGA, BPGA, GAP, DHAP, FBP, F6P, G6P, G1P, SBP, S7P, E4P, X5P, R5P, RU5P, RUBP) and a total of 19 reactions, see scheme \ref{fig:cbb_cycle}.

The model distinguishes between a set of reactions assumed to be at equilibrium (\ref{eq:eq_reactions}) and a set assumed to be displaced from equilibrium (\ref{eq:non_eq_reactions}). This is done since it is assumed that only non-equilibrium steps are of primary regulatory interest \cite{Pettersson1988}.

\begin{equation}\label{eq:eq_reactions}
\begin{split} 
	[BPGA][ADP] &= q_2 [PGA][ATP] \\
	[GAP][NADP^+][P_i] &= q_3 [BPGA][NADPH][H^+] \\
	[DHAP] &= q_4 [GAP] \\
	[FBP] &= q_5 [GAP][DHAP] \\
	[X5P][E4P] &= q_7 [GAP][F6P] \\
	[SBP] &= q_8 [DHAP][E4P] \\
	[X5P][R5P] &= q_10 [GAP][S7P] \\
	[Ru5P] &= q_{11} [R5P] \\
	[Ru5P] &= q_{12}[X5P] \\
	[G6P] &= q_{14} [F6P] \\
	[G1P] &= q_15 [G6P]	
\end{split}
\end{equation}



Each of the non-equilibrium reactions contains regulatory terms $R(S)$, giving the canonical expression

\begin{equation}
	v = \frac{V_{Max} [S]}{[S] + K_m \cdot R(S)}
\end{equation}

which together with the assumed regulatory mechanism yields the following expressions:

\begin{equation}\label{eq:non_eq_reactions}
	\begin{split}
	v_1 &= \mathrm{\frac{V_1 [RuBP]}{[RuBP] + K_{m1} \left( 1 + \frac{[PGA]}{K_{i11}} + \frac{[FBP]}{K_{i12}} + \frac{[SBP]}{K_{i13}} + \frac{[P_i]}{K_{i14}} + \frac{[NADPH]}{K_{i15}}\right)}} \\
	v_6 &= \mathrm{\frac{V_6 [FBP]}{[FBP] + K_{m6} \left( 1 + \frac{[F6P]}{K_{i61}} + \frac{[P_i]}{K_{i62}}\right)}} \\
	v_9 &= \mathrm{\frac{V_9 [SBP]}{[SBP] + K_{m9} \left(1 + \frac{[P_i]}{K_{i9}}\right)}} \\
	v_{13} &= \mathrm{\frac{V_{13}[Ru5P][ATP]}{a \cdot b}} \\
	& \qquad \mathrm{with} \\
	& \qquad a = \left[[Ru5P] + K_{m131} \left(1 + \frac{[PGA]}{K_{i131}} + \frac{[RuBP]}{K_{i132}} + \frac{[P_i]}{K_{i133}} \right) \right] \\
	& \qquad b = \left[ [ATP] \left( 1 + \frac{[ADP]}{K_{i134}} \right) + K_{m132} \left(1 + \frac{[ADP]}{K_{i135}} \right) \right] \\
	v_{16} &= \frac{V_{16} [ADP][P_i]}{([ADP] + K_{m161}) \cdot ([P_i] + K_{m162})}
	\end{split}
\end{equation}

Besides the simplified starch production pathway ($v_{16}$), the model has triose phosphate translocators as further system outputs.


\begin{equation}
\begin{split}
	v_{PGA} &= \frac{V_{ex} [PGA]}{N \cdot K_{PGA}} \\
	v_{GAP} &= \frac{V_{ex} [GAP]}{N \cdot K_{GAP}} \\
	v_{DHAP} &= \frac{V_{ex} [DHAP]}{N \cdot K_{DHAP}} \\
\end{split}
\end{equation}

with

\begin{equation}
\begin{split}
	\mathrm{N} &= \mathrm{1 + \left(1 + \frac{K_{P_{ext}}}{[P_{ext}} \right) \cdot \left( \frac{[P_i]}{K_{P_i}} + \frac{[PGA]}{K_{PGA}} + \frac{[GAP]}{K_{GAP}} + \frac{[DHAP]}{K_{DHAP}}\right)} \\
\end{split}
\end{equation}

Lastly, the model contains the following three conserved quantities, which are used to track the ATP and inorganic phosphate concentration and the total amount of exported triose phosphates.  

\begin{equation}
\begin{split}
	\mathrm{AP_{total}} &= \mathrm{ADP + ATP} \\
	\mathrm{P_{total}} &= \mathrm{P_i} + \mathrm{([PGA] + 2 \cdot [BPGA] + [GAP] + [DHAP] + }\\
	&\qquad \qquad \qquad \mathrm{2 \cdot [FBP] + [F6P] + [G6P] + [G1P] + }\\
	&\qquad \qquad \qquad \mathrm{2 \cdot [SBP] + [S7P] + [E4P] + [X5P] + }\\
	&\qquad \qquad \qquad \mathrm{[R5P] + 2 \cdot [RuBP] + [Ru5P] + [ATP])}
\end{split}
\end{equation}


Combined with the stoichiometry of the model this leads to the following system of ODEs:

\begin{align*}
\frac{d\mathrm{PGA}}{\mathrm{dt}} &= 2 \cdot v_1 - v_2 - v_{\mathrm{PGA}} \\
\frac{d\mathrm{BPGA}}{\mathrm{dt}} &= v_2 - v_3 \\
\frac{d\mathrm{GAP}}{\mathrm{dt}} &= v_3 - v_4 - v_5 - v_7 - v_{10} - v_{\mathrm{GAP}} \\
\frac{d\mathrm{DHAP}}{\mathrm{dt}} &= v_4 - v_5 - v_8 - v_{\mathrm{DHAP}} \\
\frac{d\mathrm{FBP}}{\mathrm{dt}} &= v_5 - v_6 \\
\frac{d\mathrm{F6P}}{\mathrm{dt}} &= v_6 - v_7 - v_{14} \\
\frac{d\mathrm{E4P}}{\mathrm{dt}} &= v_7 - v_8 \\
\frac{d\mathrm{SBP}}{\mathrm{dt}} &= v_8 - v_9 \tag{\stepcounter{equation}\theequation}\\
\frac{d\mathrm{S7P}}{\mathrm{dt}} &= v_9 - v_{10} \\
\frac{d\mathrm{X5P}}{\mathrm{dt}} &= v_7 + v_{10} - v_{12} \\
\frac{d\mathrm{R5P}}{\mathrm{dt}} &= v_{10} - v_{11} \\
\frac{d\mathrm{Ru5P}}{\mathrm{dt}} &= v_{11} + v_{12} - v_{13} \\
\frac{d\mathrm{RuBP}}{\mathrm{dt}} &= v_{13} - v_1 \\
\frac{d\mathrm{G6P}}{\mathrm{dt}} &= v_{14} - v_{15} \\
\frac{d\mathrm{G1P}}{\mathrm{dt}} &= v_{15} - v_{\mathrm{Starch}} \\
\frac{d\mathrm{ATP}}{\mathrm{dt}} &= v_{16} - v_2 - v_{13} - v_{\mathrm{Starch}} \\
\end{align*}


\section{Poolman variant}\label{sec:poolman-variant}
The Poolman variant, on the other hand, does not assume that any reactions attain equilibrium and uses simple, reversible mass action kinetics to model the reactions previously assumed to be at equilibrium, described by rate function \ref{eq:poolman_k}

\begin{equation} \label{eq:poolman_k}
	v = k_{re} \left( \prod_{i = 1}^{n_s} S_i - \frac{\prod_{j = 1}^{n_p}P_j}{K_{eq}} \right)
\end{equation} 

where $k_{re}$ is the reaction constant, $\prod_{i = 1}^{n_s} S_i$ is the product over all ${n_s}$ reaction substrates and $\prod_{j = 1}^{n_p}P_j$ is the product over all ${n_p}$ reaction products. The value for $k_{re}$ was set to $5\cdot 10^8$ for all reactions, regardless of the molecularity of the reaction. This introduces the following explicit rates for these reactions.

\begin{align*}
v_2 &= k_{re} \cdot \left(\mathrm{[PGA][ATP] - \frac{[BPGA][ADP]}{q_2}}\right) \\
v_3 &= k_{re} \cdot \left(\mathrm{[BPGA][NADPH][H^+] - \frac{[GAP][NADP^+][P_i]}{q_3}}\right) \\
v_4 &= k_{re} \cdot \left(\mathrm{[GAP] - \frac{[DHAP]}{q_4}}\right) \\
v_5 &= k_{re} \cdot \left(\mathrm{[GAP][DHAP] - \frac{[FBP]}{q_5}}\right) \\
v_7 &= k_{re} \cdot \left(\mathrm{[GAP][F6P] - \frac{[X5P][E4P]}{q_7}}\right) \\
v_8 &= k_{re} \cdot \left(\mathrm{[DHAP][E4P] - \frac{[SBP]}{q_8}}\right) \tag{\stepcounter{equation}\theequation}\\
v_{10} &= k_{re} \cdot \left(\mathrm{[GAP][S7P] - \frac{[X5P][R5P]}{q_{10}}}\right) \\
v_{11} &= k_{re} \cdot \left(\mathrm{[R5P] - \frac{[Ru5P]}{q_{11}}}\right) \\
v_{12} &= k_{re} \cdot \left(\mathrm{[X5P] - \frac{[Ru5P]}{q_{12}}}\right) \\
v_{14} &= k_{re} \cdot \left(\mathrm{[F6P] - \frac{[G6P]}{q_{14}}}\right) \\
v_{15} &= k_{re} \cdot \left(\mathrm{[G6P] - \frac{[G1P]}{q_{15}}}\right) \\
v_{15} &= k_{re} \cdot \left(\mathrm{[G6P] - \frac{[G1P]}{q_{15}}}\right) \\
\end{align*}


\section{Labeled CBB model}\label{sec:labeled-cbb-model}
The labeled model was created by introducing compounds for all possible label combinations and then using carbon maps in order to correctly route the compounds for each reaction. The mechanism behind this is shown in figure \ref{fig:gapdhapcarbonmap}. In the case of a three carbon molecule like GAP, there are $2^3$ possible labeling states. In total this results in 688 carbon containing compounds and ATP, not including the conserved quantities, and 4233 reactions. Since the total amount of each carbon containing compounds is now also tracked as a conserved quantity, the total amount of compounds is 707 and the total amount of conservation equations is 18. 
Table \ref{table:carbon-maps} shows the carbon maps of RuBisCO, TPI, Transketolase and Aldolase, which are the only reactions introducing changes in labeling patterns. The carbon maps for all other reactions map each carbon position of the substrate to the same position in the product.
% are the ranges of ascending numbers to the total amount of carbons in that reaction.
A requirement for label simulations in this work is that the system is in a steady state ($\dot{x} = \mathrm{N} \vec{v} = 0$). This is not a technical requirement, but reflects the fact that biological experiments with isotopic labeling are performed on living plants or cells, which are assumed to have a steady state CBB cycle. This assumption is discussed in \ref{sec:steady-state-assumption}.
Since the half-life of $\ce{^{14}C}$ is $5,730 \pm 40$ years and thus way beyond the simulation times, no degradation terms were introduced.

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{Images/gap-dhap-carbon-map.pdf}
	\caption[Carbon map explanation]{Carbon map of GAP to DHAP conversion. The corresponding carbon atoms are colored the same. The conversion of the compounds corresponds to the carbon map \mintinline{python3}{[2, 1, 0]}, using Python list indexing. This means that the C1 atom of GAP is transferred to the 3 position of DHAP, and the C3 carbon of GAP is transferred to the 1 position of DHAP, while the C2 position remains unchanged. If a putative GAP molecule is labeled only in the first position, the corresponding compound is GAP100, the conversion would create a DHAP labeled in the third position, DHAP001.}
	\label{fig:gapdhapcarbonmap}
\end{figure}

\begin{table}[h]
	\centering
	\begin{tabular}{l l l}
	Reaction carbon compounds & Forward & Backward \\
	v1: RuBP + $CO_2$ \ce{<=>} PGA + PGA & 2, 1, 0, 5, 3, 4 & \  \\
	v4: GAP \ce{<=>} DHAP & 2, 1, 0 & 2, 1, 0 \\
	v5: DHAP + GAP \ce{<=>} FBP & 0, 1, 2, 3, 4, 5 & 0, 1, 2, 3, 4, 5 \\
	v7: F6P + GAP \ce{<=>} X5P + E4P & 0,1,6,7,8,2,3,4,5 & 0,1,5,6,7,8,2,3,4 \\
	v8: DHAP + E4P \ce{<=>} SBP & 0,1,2,3,4,5,6 & 0,1,2,3,4,5,6 \\
	v10: S7P + GAP \ce{<=>} X5P + R5P & 0,1,7,8,9,2,3,4,5,6 & 0,1,5,6,7,8,9,2,3,4 \\
	\end{tabular}
	\caption[Carbon maps of the reactions that introduce changes in labeling patterns]{Carbon maps of the reactions that introduce changes in labeling patterns. Reaction RuBP + $CO_2$ \ce{<=>} PGA + PGA corresponds to v1, which is modeled as an irreversible reaction and thus has no backward carbon map.}
	\label{table:carbon-maps}
\end{table}

As can be seen in figure \ref{fig:poolman-fbp-label-distribution}, the label distribution of the Poolman model shows neither the measured $\frac{C4}{C3}$, nor $\frac{C1}{C6}$ and $\frac{C1}{C6}$ asymmetries. Under the hypothesis, that the $\frac{C4}{C3}$ asymmetry emerges from the different GAP and DHAP pool sizes and thus the different level of percentual label concentration, a very fast forward and backward reaction between those compounds will void that effect, by keeping the level of percentual label concentration equal. While those extremely fast rate constants are able to closely mirror the model proposed by Pettersson, in the author's view they are not biologically meaningful. Thus, the parameters were fitted to concentrations and free energies measured by Bassham \cite{Bassham1969}, as described in section \ref{sec:rate-parameters}. While this alteration drastically reduces the models ability to react to changes in external orthophosphate, fig. \ref{fig:stability-bassham}, it reproduces observed label asymmetries. Since the focus of this work lies on the labeling pattern, the parameter set was accepted. But the fact that the model looses much of its flexibility once parameters that are not biologically meaningful are replaced by ones that are, is a major issue of the Poolman implementation. A possible explanation for this behaviour could lie in the regulation terms of the non-equilibrium reactions, but more analysis is needed to clarify this. 

\subsection{Parameter fitting}\label{sec:parameter-fitting}
During this work it became necessary to replace the fast rate constant $k_{re}$, as the hypothesis was formed, that the very fast forward and backward reactions caused by the rate constant prevented any observable label asymmetries by rapid equilibration of label pools. This is further discussed in section \ref{sec:rate-parameters}. The fast rate constant $k_{re}$ introduced by Poolman was therefore replaced with individual rate constants for each reaction. This was done using a full set of measured concentrations, as well as $\Delta G$ and $\Delta G^0$ values, obtained from Bassham 1969 \cite{Bassham1969} and the steady state fluxes obtained by simulating the Poolman model fluxes using the following identities:

\begin{equation}\label{eq:ma_rate_params_forward}
\begin{split}
&\phantom{\Leftrightarrow \ } v^+ - v^- = v \\ 
&\Leftrightarrow k^+ \prod_i S_i - k^- \prod_j P_j = v \\ 
&\Leftrightarrow k^+ \left( \prod_i S_i - \frac{k^-}{k^*} \prod_j P_j \right) = v \\
&\Leftrightarrow k^+ = \frac{v}{\prod_i S_i - \frac{k^-}{k^+} \prod_j P_j} \\
&\Leftrightarrow k^+ = \frac{v}{\prod_i S_i \left( 1 - \frac{k^-}{k^+} \frac{\prod_j P_j}{\prod_i S_i} \right) } \\
&\Leftrightarrow k^+ = \frac{\frac{v}{\prod_i S_i}}{1 - \frac{k^-}{k^+} \cdot Q} \\
&\Leftrightarrow k^+ = \frac{\frac{v}{\prod_i S_i}}{1 - \frac{Q}{K_{eq}}}
\end{split}
\end{equation}

likewise

\begin{equation}\label{eq:ma_rate_params_backward}
\begin{split}
%v^+ - v^- &= v \\ %&&| \; v^+ = k^+ \prod_i S_i, v^- = k^- \prod_j P_j 
%k^+ \prod_i S_i - k^- \prod_j P_j &= v \\ 
%k^- \left(\frac{k^+}{k^-} \prod_i S_i - \prod_j P_j \right) &= v \\
%k^- &= \frac{v}{\frac{k^+}{k^-} \prod_i S_i - \prod_j P_j} \\
%k^- &= \frac{\frac{v}{\prod_j P_j}}{\frac{k^+}{k^-} \frac{\prod_i S_i}{\prod_j P_j} - 1} \\ %&&|\; Q = \frac{\prod_j P_j}{\prod_i S_i}
%k^- &= \frac{\frac{v}{\prod_j P_j}}{\frac{k^+}{k^-} \frac{1}{Q} - 1} \\ %&&| \; K_{eq} = \frac{k^+}{k^-} 
&\phantom{\Leftrightarrow \ } k^- = - \frac{\frac{v}{\prod_j P_j}}{1 - \frac{K_{eq}}{Q}}
\end{split}
\end{equation}


%\begin{equation}
%\Delta G = \Delta G^0 + R \cdot T \cdot ln(Q)
%\end{equation}
%
%\begin{equation}
%\Delta G^0 = - R \cdot T \cdot ln(K_{eq})
%\end{equation}

With the new obtained mass action rate parameters the corresponding reactions were brought into standard mass action format

\begin{equation}\label{eq:standard-mass-action}
	v = k^+ \prod_{i}S_i - k^- \prod_{j} P_j
\end{equation}

Similiarly, in order to fit the regularised reactions that follow the canonical expression

\begin{equation}
	v = \frac{V_{Max} \cdot [S]}{[S] + K_m \cdot R(S)}
\end{equation}

the $V_{Max}$ values were obtained using 

\begin{equation}
	V_{Max} = \frac{v \left( [S] + K_m \cdot R(S) \right)}{[S]}
\end{equation}

and the experimentally measured concentrations.

\section{Python and analyses}
To set up the differential equation system, the model was implemented in the programming language Python. The numerical simulations were performed using the CVode integrator of the sundials solver suite \cite{Hindmarsh2005}, which was called using the wrapper package assimulo \cite{Andersson2015}. To display the results the matplotlib library was used.

The full project can be found at \href{https://gitlab.com/marvin.vanaalst/labeled-calvin-cycle}{gitlab}.


\subsection{External orthophosphate stability analysis}
In order to reproduce original work by Pettersson and Ryde-Pettersson \cite{Pettersson1988}, a scan over the external orthophosphate concentration $P_{ext}$ was performed, which was linearly varied between $10^{-3}$ and 2 mM with 512 total steps. Each integration was performed until a time point of 100000, which was used as the steady state criteria, with an absolute and relative tolerance of $10^{-8}$ each. The maximal number of convergence failures was set to 1 and the maximal number of error test failures to 3, in order to minimize computational time concentrations of $P_{ext}$ on which the system is unstable. 


\subsection{Asymmetries calculation}
In order for the label asymmetries to be calculated, the concentrations of labeled compounds obtained by numerical integration were interpolated with numpys \mintinline{python3}{numpy.interp} routine. Values outside the measured range were replaced with numpys NaN special value \mintinline{python3}{numpy.NaN}. Afterwards the according fractions were calculated and returned as numpy arrays.

\subsection{Enzyme scans}
To observe the effect of enzyme concentration and thus the resulting changed compound concentrations on the labeling pattern, a scan over all reactions was performed. In the case of the reactions described by mass action kinetics, the reaction term was multiplied by a factor $i$, resulting in the canonical expression $v = i \cdot \left(k^+ \prod_{i}S_i - k^- \prod_{j} P_j\right)$. In the case of reactions described by Michaelis-Menten kinetics, the $V_{Max}$ of the reaction was multiplied by a factor $i$, resulting in the canonical expression $v = \frac{V_{Max} \cdot i \cdot [S]}{[S] + K_m \cdot R(S)}$. \\
For each enzyme (combination), first a model without labeling was simulated until the time point 100000, where a steady state was assumed to have been reached. The maximal number of convergence failures was set to 3 and the maximal number of error test failures to 4 to minize computational time on simulations with parameters leading to unstable systems. The triose phosphate translocator ($vPGA_{out}$, $vGAP_{out}$ and $vDHAP_{out}$), and the starch production ($vSt$) reactions were used as a measure for system stability, as they represent the possible carbon outflux possibilities of the model. The integration range for the labeled model was determined by only using parameter combinations, for which the sum of triose phosphate translocator and starch production flux was larger or equal to a tenth of the base model flux. This was done in order to exclude numerical artifacts of unstable systems. The cutoff value was chosen in a way that the solutions were still considered as being biologically feasible.  For each parameter the new steady state was calculated and the label model was then integrated using the calculated steady state as initial conditions.
\chapter{Discussion}

\section{Rate parameters}\label{sec:rate-parameters}
As can be seen in figure \ref{fig:poolman-fbp-label-distribution}, the label distribution of the Poolman model shows neither the measured $\frac{C4}{C3}$, nor $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries. Under the hypothesis that the $\frac{C4}{C3}$ asymmetry emerges from the different GAP and DHAP pool sizes and thus the different level of percentual label concentration, a very fast forward and backward reaction between those compounds will void that effect, by keeping the level of percentual label concentration equal. While those extremely fast rate constants are able to closely mirror the model proposed by Pettersson, in the author's view they are not biologically meaningful. Thus, the parameters were fitted to concentrations and free energies measured by Bassham \cite{Bassham1969}, as described in section \ref{sec:parameter-fitting}. While this alteration drastically reduces the model's ability to react to changes in external orthophosphate, fig. \ref{fig:stability-bassham}, it reproduces observed label asymmetries. Since the focus of this work lies on the labeling pattern, the parameter set was accepted. But the fact that the model looses much of its flexibility once parameters that are not biologically meaningful are replaced by ones that are, is a major issue of the Poolman implementation. A possible explanation for this behaviour could lie in the regulation terms of the non-equilibrium reactions, but more analysis is needed to clarify this. 

\section{Steady state assumption}\label{sec:steady-state-assumption}
A steady state CBB cycle is assumed for all label simulations. However, Gibbs and Kandler often switched between 5 \cite{Kandler1956,Gibbs1957} or 15 \cite{Gibbs1951} minutes of photosynthesis prior to introduction of $\ce{^{14}C}$ and 5 min incubation with $\ce{^{14}C}$ in the dark prior to illuminating for varying amounts of time, the lowest being 60s \cite{Gibbs1957}. Even though the label asymmetries did not change qualitatively in the measured cases, it still remains a confounding variable. Especially since under the hypothesis that asymmetries may depend on pool sizes, dynamically changing the pool sizes through illumination at the beginning of the experiment will probably influence the measured effect. Therefore more systematic measurements are needed.

\section{Labeling}\label{sec:labeling}
In all compounds, one or several main labels can be observed. Bassham noted in 1954 that this behaviour might be due to substrate channeling \cite{Bassham1954}. However, since no substrate channeling is present in this model and the effect still persists, it is more probable that it is due to the rapid speed of interconversion of compounds in comparison to the full cycle speed.
The labeling pattern of PGA, BPGA, GAP and DHAP, observed in figure \ref{fig:bassham-all-compounds}, is exactly as expected. The labeled $\ce{CO_2}$ is inserted into the first position of PGA, passed on through BGPA$^1$ and GAP$^1$ and then passed to DHAP$^3$. The second and third (first for DHAP) positions require a labeled RuBP$^1$ and RuBP$^2$ to re-enter the cycle respectively and are thus labeled less than the first (third for DHAP) position. The proposed reason for the asymmetry between positions 2 and 3 and the following implications are discussed in section \ref{sec:c1-c2-and-c5-c6}.
FBP, F6P, G6P and G1P show an $\frac{C4}{C3}$ asymmetry that is further discussed in section \ref{sec:c4-c3-asymmetry}. The $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries of these compounds are discussed in section \ref{sec:c1-c6-c2-c5-asymmetry}, and the $\frac{C1}{C2}$ and $\frac{C5}{C6}$ asymmetries in section \ref{sec:c1-c2-and-c5-c6}. The low relative labeling of G6P and G1P is a result of the low model flux through the starch production pathway, as in the model only $8.1 \%$ of the incorporated carbons is converted to starch, while $91.9 \%$ is exported as triose phosphates. Since the label pattern does not change between F6P and G6P or G1P, F6P was chosen as the reference molecule, in order to circumvent the need for long integration time spans.
% E4P
E4P is mainly labeled in C1 and C2, whereby the concentration of E4P$^2$ is larger than the concentration of E4P$^1$. This is a direct result of the $\frac{C4}{C3}$ asymmetry of F6P, as E4P$^2$ comes from F6P$^4$ and E4P$^1$ from F6P$^3$.
% SBP and S7P
SBP and S7P are labeled mostly in C3, then C5 and C4, however the C5 atom of S7P is labeled more than the C5 atom of SBP. S7P can be built from S7P and from X5P and R5P by a reverse transketolase reaction, thus it has an additional production route compared to SBP. In the shared production route the C3 label comes from DHAP$^3$, while C4 and C5 come from carbons C1 and C2 of E4P.The higher C3 label can be explained by DHAP showing a higher relative label concentration than E4P (roughly twice at $t = 2$ [au]). The $\frac{C5}{C4}$ asymmetry on the other hand is a direct result of the $\frac{C2}{C1}$ asymmetry of E4P and thus the $\frac{C4}{C3}$ asymmetry of FBP and F6P. In the production route unique to S7P however, carbons 3-5 of S7P are derived from carbonss 1-3 of R5P, which can be produced from X5P by the pathway $\ce{\text{X5P} -> \text{Ru5P} -> \text{R5P}}$. X5P can be created by the transketolase reaction $\ce{\text{GAP} + \text{F6P} <=> \text{X5P} + \text{E4P}}$ with X5P$^3$ corresponding to GAP$^1$, which is highly labeled, and X5P$^12$ corresponding to F6P$^12$. Since the third position of X5P is labeled more than the first two and ultimately gets inserted into the fifth position of S7P, the increase in $\frac{C5}{C4}$ asymmetry from SBP to S7P is caused by the reverse transketolase reaction as the SBPase reaction is modeled as not being reversible and thus this increased asymmetry cannot backpropagate in the model. A possible way to test this hypothesis would be to increase the transketolase activity and measure the difference in $\frac{C5}{C4}$ asymmetry from SBP to S7P, which should increase. Another possible experiment would be decreasing the activity of RPI ($\text{R5P} \ce{<=>} \text{Ru5P}$) and RPE ($\text{X5P} \ce{<=>} \text{Ru5P}$), in which case the difference in $\frac{C5}{C4}$ asymmetry should decrease, as the label of X5P should be not be transfered to R5P.  
% X5P, R5P, Ru5P, RuBP
The labeling patterns of X5P, R5P, Ru5P and RuBP are almost identical, with the main labels in descending order being C3, C1 and C2. The exception being R5P, which shows a slightly increased C1 label. Since carbons R5P$^{123}$ correspond to S7P$^{345}$ and therefore SBP$^{345}$, the hypothesis would be, that C1 of R5P would be labeled the strongest, then C3 and then C2, according to the SBP labeling pattern. However, in all transketolase reactions X5P$^3$ can get the label of GAP$^1$, and there is a possible route of exchanging this label by 

\begin{equation}
	X5P \ce{<=>} Ru5P \ce{<=>} R5P
\end{equation}

This would suggest that the majority of R5P molecules is not labeled in C1, C2 and C3 at the same time, but only in C3 and that the interconversion of X5P, Ru5P and R5P is fast in comparison to the completion of the cycle. Figure \ref{fig:r5p-label-composition} shows this behavior, with R5P$^3$ being the main labeled R5P, followed by R5P$^1$. 

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.9\textwidth]{Plots/r5p-label-composition.pdf}
	\caption[R5P label composition]{R5P relative label composition. Only molecules with a relative label concentration > 0.05 were included in the plot.}
	\label{fig:r5p-label-composition}
\end{figure}

The long time label behaviour of FBP, F6P, G6P and G1P shown in figure \ref{fig:bassham-asymmetries-over-time} differs in the $\frac{C2}{C5}$ and $\frac{C1}{C6}$ asymmetries for FBP, as discussed in \ref{sec:c1-c6-c2-c5-asymmetry}. However all asymmetries tend towards one for long time spans, which corresponds to a high relative label concentration. Since the influx of $\ce{^{14}C}$ is constant in this model, there are no degradation terms for the labels, as the half-life of $\ce{^{14}C}$ is $5,730 \pm 40$ years (\ref{sec:labeled-cbb-model}), and all label positions in the model can be labeled (\ref{fig:bassham-all-compounds}), this is expected behaviour. 



\section{Asymmetries}
\subsection{C4-C3 asymmetry}\label{sec:c4-c3-asymmetry}
As shown in figure \ref{fig:bassham-enzyme-label-distribution-tpi}, the $\frac{C4}{C3}$ asymmetry increases with a lower activity of TPI, while a higher activity leads to less asymmetry. This is in line with the argument by Bassham \cite{Bassham1964}, as with less conversion of GAP to DHAP the fraction of labeled DHAP molecules further decreases in comparison to GAP. But less conversion also leads to a smaller DHAP pool, which means that an introduced label contributes more to the percentage of labeled DHAP. In this case the slower introduction of labeled carbons is the dominant effect, but it is imaginable, that with other pool sizes and with TPI variants in different species the asymmetry may scale differently. Figure \ref{fig:c4c3-heatmap} further shows that the activity changes of SBPase and Aldolase have an anti proportional effect on the asymmetry as well, while the activity changes of Transketolase, ATPsynthase have a propotional effect. However all those effects are much less pronounced. 
The effect of Aldose activity changes may be due to the reduction of DHAP pool, as DHAP is a substrate in both Aldolase reactions. Another argument for this is that the effect of SBPase activity changes is also anti proportional, which should also lead to a decreased DHAP pool size. Transketolase on the other hand reduces the GAP pool size and thus the effect of enzyme activity changes is proportional. However all those effects are all very small in comparison to the effect of TPI activity changes.

\subsection{C1-C6 and C2-C5}\label{sec:c1-c6-c2-c5-asymmetry}
Figures \ref{fig:c2c5-heatmap} and \ref{fig:c1c6-heatmap} show that the $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries depend on multiple enzymes: TPI, Aldolase, SBPase, PRK, and the combination of RPE and transketolase. While investigations with separated transketolase and aldolase reactions are possible in this model, their biological meaning is questionable, as in both cases there is just one enzyme catalysing both reactions. Thus in this screen they were unified. 
Aldolase cannot change the label distribution pattern of whole cycle in this model, because both reactions are followed by irreversible reactions. So even if the reversible reactions are faster, the labeled carbons always remain in the positions in their respective compounds. Thus it can only amplify or diminish existing asymmetries, by changing the mixing speed of the compound pools.
The effect of increased asymmetries of a slower TPI is probably due to the reduction of the DHAP pool. In this case, even if the conversion is slower, it leads to a higher relative amount of labeled DHAP molecules.
The combination of RPE and transketolase however shows an interesting cycle property. As the scheme proposed in figure \ref{fig:cbb-cycle-c2c5-c1c6} shows, there is a second route of how C1 and C2 labels can enter F6P

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{Images/cbb-cycle-labeling-r5p-backwards.pdf}
	\caption{Proposed pathway leading to $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries.}
	\label{fig:cbb-cycle-c2c5-c1c6}
\end{figure}

which is by the reaction route $\ce{\text{R5P} -> \text{Ru5P} -> \text{X5P} ->[TK] \text{F6P}}$.

This proposed scheme can further explain the effect seen in figure \ref{fig:bassham-asymmetries-over-time}: The $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries can be observed in F6P, G6P and G1P, but not in FBP. This is because the asymmetries are created by the backwards reactions described above. Since the reaction of FBP to F6P is assumed to be irreversible, the backward reaction of F6P to FBP cannot take place. Thus asymmetries in F6P cannot backpropagate to FBP. For simpler systems, this effect may help measuring the actual irreversibility of a reaction \textit{in vivo}.

The experiments shown in figures \ref{fig:no-co2-x5p-12} and \ref{fig:no-co2-e4p-34} show this effect in isolation. In both experiments, the influx of $\ce{^{14}C}$ was changed to unlabeled $\ce{^{12}C}$ and the simulations were initiated with fully labeled X5P$^{12}$ and E4P$^{34}$ respectively. In the case of X5P$^{12}$ the reverse reaction 
\begin{equation}
	X5P^{12} + E4P\ce{->[TK]} F6P^{12} + GAP
\end{equation}
is possible additionally to the simplified forward path
\begin{equation}
\begin{split}
	& X5P^{12} \ce{->} Ru5P^{12} \ce{->} RuBP^{12} \ce{->} PGA^{23} \ce{->} BPGA^{23} \ce{->} GAP^{23}
\end{split}
\end{equation}
which results in $GAP^{23} \ce{->} FBP^{56}$ and $GAP^{23} \ce{->} DHAP^{12} \ce{->} FBP^{12}$. But as again the DHAP pool is larger than the GAP pool, the relative level of GAP$^{23}$ is larger than DHAP$^{12}$, which leads to FBP$^{56}$ being labeled more than FBP$^{12}$ in FBP. In F6P however we can see the effect of the reverse reaction, as F6P$^{12}$ is labeled more than FBP. 
The experiment with E4P$^{34}$ on the shows the same label pattern for FBP as before, but inverts the pattern for F6P. Again, there are two possible pathways, the reverse reaction
\begin{equation}
	E4P^{34} + X5P \ce{->[TK]} F6P^{56} + GAP
\end{equation}
and the simplified forward path
\begin{equation}
\begin{split}
	& E4P^{34} \ce{->} SBP^{67} \ce{->} S7P^{67} \ce{->} R5P^{45} \ce{->} Ru5P^{45} \\
	& \ce{->} RuBP^{45} \ce{->} PGA^{23} \ce{->} BPGA^{23} \ce{->} GAP^{23}
\end{split}
\end{equation}
which again leads to $GAP^{23} \ce{->} FBP^{56}$ and $GAP^{23} \ce{->} DHAP^{12} \ce{->} FBP^{12}$.

Enzyme activity changes of SBPase and PRK show anti proportional effects to both asymmetries. This is in line with the above argument and further evidence for the effect of rapid speed of interconversion of compounds in comparison to the full cycle speed, as both reactions are modeled as irreversible and pass labels along the circle instead of changing labeling patterns.


\subsection{C1-C2 and C5-C6}\label{sec:c1-c2-and-c5-c6}
While Gibbs and Kandler observed the $\frac{C4}{C3}$, $\frac{C1}{C6}$ and $\frac{C2}{C5}$ label asymmetries in all their experiments, the exact order, in which the carbons 1, 2, 5 and 6 are labeled, differed \cite{Gibbs1951,Kandler1956,Gibbs1957}. However, the asymmetries between $\frac{C1}{C2}$ and $\frac{C6}{C5}$ are not discussed, even if they can be observed, eg. \cite{Gibbs1957}. 
In section \ref{sec:models-without-labeled-co2}, in which the model does not contain a constant influx of labeled $\ce{CO_2}$, experiment \ref{fig:no-co2-rubp-12}, which was initiated with $RUBP^{12}$ shows no asymmetries within the C1-C2 and C5-C6 pairs. Figure \ref{fig:no-co2-rubp-12-asymmetrically} however, in which RUBP was labeled asymetrically in positions C1 and C2 ($\frac{2}{3}$ in position 1 and $\frac{1}{3}$ in position 2), shows the observed behaviour.\\
It is known that aldolase builds FBP by stacking DHAP on top of GAP. Thus the C1 and C2 atoms of FBP correspond to C1 and C2 of DHAP and the C5 and C6 atoms of FBP correspond to the C2 and C3 atoms of GAP. 
As discussed in section \ref{sec:labeling}, the second most labeled R5P, and thus also RuBP, is $RuBP^1$. When $RuBP^1$ re-enters the cycle, the label is inserted into the third position of the PGA molecule, into which the $\ce{CO_2}$ is not inserted, thus leading to PGA$^3$ and PGA$^1$ from $\ce{CO2}$. The PGA$^3$ label is passed to GAP$^3$, which is built into FBP into the 6 position, or can beforehand be isomerised into DHAP$^1$, which is built into the 1 position of FBP. Thus both the $\frac{C1}{C2}$ and $\frac{C5}{C6}$ asymmetries are dependent on TPI, or the amount of DHAP$^3$, with the suggested simplified pathway: 

\begin{equation}
\begin{split}
	& DHAP^3 \ce{->} SBP^3 \ce{->} R5P^1 \ce{->} RuBP^1 \ce{->} PGA^3 \\
	& \ce{->} GAP^3 \ce{->} FBP^6 \\
	& \mathrm{or} \\
	& \ce{->} GAP^3 \ce{->} DHAP^1 \ce{->} FBP^1
\end{split}
\end{equation}

Interestingly, once labels in either position 2 (2) or 3 (1) in GAP (DHAP) are introduced,  "futile" label cycles are possible:

\begin{equation}
	GAP^{23} \ce{->} X5P^{45} \ce{->} Ru5P^{45} \ce{->} PGA^{23} \ce{->} GAP^{23}
\end{equation}

\begin{equation}
	DHAP^{12} \ce{->} SBP^{12} \ce{->} X5P^{12} \ce{->} RuBP^{12} \ce{->} PGA^{23}  \ce{->} DHAP^{12}
\end{equation}



\section{Conclusion \& Outlook}
Even though the subject is complex, due to the very high amount of possible label pathways, the model enables giving a possible explanation for the photosynthetic Gibbs effect. The simulations support the argument for the $\frac{C4}{C3}$ asymmetry given by Bassham in 1964 \cite{Bassham1964} and expand the arguments given for the $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries, by giving a designated reaction scheme. Further, the $\frac{C1}{C2}$ and $\frac{C5}{C6}$ asymmetries, that were measured, but not further discussed, can be explained by the model as well. However, the model was only fitted to measured concentrations, not to measured asymmetries, as there is very little data available on it. 
As mentionend in the introduction, see sec. \ref{sec:the-photosynthetic-gibbs-effect}, it is possible that transketolase might also catalyse the non-standard conversion of S7P + E4P $\ce{<=>}$ F6P + R5P, neutral conversions like X5P + GAP $\ce{<=>}$ X5P + GAP or reactions including octoses. In the case of the neutral reactions, the conversions for e.g. X5P + GAP $\ce{<=>}$ X5P + GAP could further increase the speed with which the X5P$^3$ label forms, as it adds another production route, but otherwise their effect is neutral on the label patterns as well. 
In the case of the non-standard reaction of S7P + E4P $\ce{<=>}$ F6P + R5P I don't think that the effect on the label distribution will be significant either. The formation of R5P from S7P carbons 3-7 also happens in the standard transketolase reaction S7P + GAP $\ce{<=>}$ X5P + R5P and for the reverse reaction the formation of E4P from carbon atoms 3-6 of F6P also happens in the standard transketolase reaction F6P + GAP $\ce{<=>}$ X5P + E4P. So while increased mixing of these positions is expected, no new patterns can be formed with this reaction. Thus the effect might be most noticeable in differences in label asymmetries between compounds, e.g. the difference in $\frac{C5}{C4}$ asymmetry between SBP and S7P might increase, but not new asymmetries can be introduced.
With the octoses however, the effect could be larger. The possible reactions are

\begin{equation}
\begin{split}
	\mathrm{O8P + GAP} &\ce{<=>} \mathrm{X5P + G6P} \\
	\mathrm{O8P + E4P} &\ce{<=>} \mathrm{F6P + G6P} \\
	\mathrm{O8P + R5P} &\ce{<=>} \mathrm{S7P + G6P} \\
	\mathrm{O8P + G6P} &\ce{<=>} \mathrm{O8P + G6P} \\
\end{split}
\end{equation}

thus the octose carbon atoms 3-8 always correspond to G6P. Therefore the first testable hypothesis is that O8P$^{6}$ is labeled more than O8P$^{6}$ (G6P $\frac{C4}{C3}$ asymmetry), O8P$^{3}$ is labeled more than O8P$^{8}$ (G6P $\frac{C1}{C6}$ asymmetry), O8P$^{4}$ is labeld more than O8P$^{7}$ (G6P $\frac{C2}{C5}$ asymmetry) and also that O8P$^{3}$ is labeled more than O8P$^{4}$ (G6P $\frac{C1}{C2}$ asymmetry) and that O8P$^{8}$ is labeled more than O8P$^{7}$ (G6P $\frac{C6}{C5}$ asymmetry). The C1 and C2 positions of O8P however, get exchanged with the C1 and C2 positions of X5P, F6P and S7P respectively. As figure \ref{fig:c1-c2-positions} shows, those two positions are labeled more in X5P than in S7P and F6P. Thus an interconversion with O8P could increase the relative labeling in those positions for S7P and F6P, increasing the $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries in F6P, G6P and G1P.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{Plots/C1-C2-positions}
	\caption[First two carbon positions in F6P, S7P and X5P]{Concentration of the first two carbon positions in F6P, S7P and X5P, relative to each compounds total concentration.}
	\label{fig:c1-c2-positions}
\end{figure}

Another possible reaction would be transaldolase, which catalyses the reversible reaction S7P + GAP $\ce{<=>}$ E4P + F6P, in which the first three carbons of S7P are cleaved, and combined with GAP to form F6P, with E4P as the remainder of S7P. F6P$^{123}$ correspond to S7P$^{123}$ and F6P$^{456}$ to GAP$^{123}$. Given the results of this thesis, the main labeled carbons in S7P are assumed to be C3, C5 and C4 in descending order and the main label of GAP is assumed to be C1. In this case S7P$^{45}$ does not change the existing main label in E4P, E4P$^{12}$, nor does it change the observed $\frac{C2}{C1}$ asymmetry, as S7P$^{5}$ is labeled more than S7P$^{4}$. In the case of F6P carbon atoms C1 and C2 are exchanged with C1 and C2 of S7P, which are labeled similarly. The carbon atoms C5 and C6 are exchanged with C2 and C3 of GAP. This might lead to a reduction of the $\frac{C1}{C6}$ and $\frac{C2}{C5}$ asymmetries, along the line of argument presented explaining the occurrence of those asymmetries. \ref{sec:c1-c6-c2-c5-asymmetry}. The $\frac{C4}{C3}$ asymmetry on the other hand should further increase, as F6P$^{4}$ is exchanged with the strongly labeled GAP$^{1}$ F6P$^{3}$ with the less labeled S7P$^{3}$. However, as those are the most labeled positions in the respective compounds, i assume that there will not be any observable difference in GAP and S7P by introduction of transaldolase to the model. 
Modern scientific fields, e.g. synthetic biology, provide methods to systematically scan ranges of enzyme activity \textit{in vivo}, by using promotor libraries and gene or genome editing techniques. Thus predictions by this model regarding the effect of different enzyme activity inside the organism on label asymmetries can in principle, and in the honest opinion of the author, should be tested, as this will help in solving the debate over the exact structure of the Calvin-Benson-Bassham cycle.
 
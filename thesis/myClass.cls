\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{myClass}

% Use existing stuff
\LoadClass[12pt, a4paper, twoside, openany]{book}

% Remove paragraph indent
\setlength{\parindent}{0pt} 

% Graphics
\RequirePackage{graphicx}

%
%\RequirePackage{minted}

%----------------------------------------------------------------------------------------
%	MARGINS
%----------------------------------------------------------------------------------------
\RequirePackage{geometry}
\geometry{
%	headheight=4ex,
%	includehead,
%	includefoot,
%	paper=a4paper,
	inner=2.5cm, % Inner margin
	outer=2cm, % Outer margin
	bindingoffset=0.5cm, % Binding offset
	top=2.5cm, % Top margin
	bottom=2.5cm, % Bottom marg	in
%	showframe, % Uncomment to show how the type block is set on the page
}

%----------------------------------------------------------------------------------------
%	DEFINE CUSTOM THESIS INFORMATION COMMANDS
%----------------------------------------------------------------------------------------
%\RequirePackage{xparse}
%\NewDocumentCommand{\supervisor}{m}{\newcommand{\supname}{#1}}


%----------------------------------------------------------------------------------------
%	COLOURS
%----------------------------------------------------------------------------------------

\usepackage{xcolor} % Required for specifying custom colours
\definecolor{C1}{HTML}{438ecc}
\definecolor{C2}{HTML}{1eb8d1}
\definecolor{C3}{HTML}{009688}
\definecolor{C4}{HTML}{4daf50}
\definecolor{C5}{HTML}{ccd73f}
\definecolor{C6}{HTML}{f4971b}
\definecolor{C7}{HTML}{ea592d}


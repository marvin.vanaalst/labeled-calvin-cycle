# Contains:
# Poolman
# SplitPoolman
# Bassham


class Poolman:
    ###########################################################################
    # Algebraic
    ###########################################################################
    def ADP(self, p, ATP):
        return [p.Ca - ATP]

    def P_i(
        self,
        p,
        PGA,
        BPGA,
        GAP,
        DHAP,
        FBP,
        F6P,
        G6P,
        G1P,
        SBP,
        S7P,
        E4P,
        X5P,
        R5P,
        RUBP,
        RU5P,
        ATP,
    ):
        return [
            p.Cp
            - (
                PGA
                + 2 * BPGA
                + GAP
                + DHAP
                + 2 * FBP
                + F6P
                + G6P
                + G1P
                + 2 * SBP
                + S7P
                + E4P
                + X5P
                + R5P
                + 2 * RUBP
                + RU5P
                + ATP
            )
        ]

    def N(self, p, P, PGA, GAP, DHAP):
        """Used to calculate vPGA_out, vGAP_out and vDHAP_out"""
        return [
            (
                1
                + (1 + (p.Kpxt / p.Pext))
                * ((P / p.Kpi) + (PGA / p.Kpga) + (GAP / p.Kgap) + (DHAP / p.Kdhap))
            )
        ]

    ###########################################################################
    # Reaction rates
    ###########################################################################

    def v1(self, p, RUBP, PGA, FBP, SBP, P):
        """Irreversible reaction: RuBP + CO2 -> PGA

        3 Ribulose-1,5-bisphosphate + 3 CO2
        -- RuBisCO -->
        6 3-Phosphoglycerate
        """
        return (p.V1 * RUBP) / (
            RUBP
            + p.Km1
            * (
                1
                + (PGA / p.Ki11)
                + (FBP / p.Ki12)
                + (SBP / p.Ki13)
                + (P / p.Ki14)
                + (p.NADPH / p.Ki15)
            )
        )

    def v2(self, p, ATP, PGA, ADP, BPGA):
        """Reversible, fast EQ reaction : PGA + ATP <-> BPGA + ADP

        6 3-Phosphoglycerate + 6 ATP
        -- Phosphoglycerate kinase (PGK) -->
        6 1,3-Bisphosphoglycerate + 6 ADP
        """
        return p.kRE * ((ATP * PGA) - (ADP * BPGA) / p.q2)

    def v3(self, p, BPGA, GAP, Phosphate_i):
        """Reversible, fast EQ reaction: BPGA + NADPH <-> GAP + NADP

        6 1,3-Bisphosphoglycerate + 6 NADPH + 6 H+
        -- Glyceraldehyde 3-phosphate dehydrogenase (GADPH)-->
        1 G3P + 5 Glyceraldehyde 3-phosphate

        Stroma pH is assumed to be constant
        """
        return p.kRE * (
            (p.NADPH * BPGA * p.protonsStroma) - (1 / p.q3) * (GAP * p.NADP * Phosphate_i)
        )

    def v4(self, p, GAP, DHAP):
        """Reversible, fast EQ reaction: GAP <-> DHAP

        (5) Glyceraldehyde 3-phosphate
        -- Triose phosphate isomerae (TPI)-->
        (?) Dihydroxyacetone phosphate
        """
        return p.kRE * ((GAP) - (DHAP) / p.q4)

    def v5(self, p, GAP, DHAP, FBP):
        """Reversible, fast EQ reaction: GAP + DHAP <-> FBP

        (5) Glyceraldehyde 3-phosphate + (?) Dihydroxyacetone phosphate
        -- Aldolase (ALD)-->
        Frucose 1,6-bisphosphate
        """
        return p.kRE * ((GAP * DHAP) - (FBP) / p.q5)

    def v6(self, p, FBP, F6P, P):
        """Irreversible reaction: FBP -> F6P

        (?) Fructose 1,6-bisphosphate + (?) H20
        --Fructose 1,6-bisphosphatase (FBPase) -->
        (6) Fructose 6-phosphate + (Pi)
        """
        return (p.V6 * FBP) / (FBP + p.Km6 * (1 + (F6P / p.Ki61) + (P / p.Ki62)))

    def v7(self, p, GAP, F6P, X5P, E4P):
        """Reversible, fast EQ reaction: GAP + F6P <-> X5P + E4P

        (?) Fructose 6-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK) -->
        (?) Xylulose 5-phosphate + (?) Erythrose 4-phosphate
        """
        return p.kRE * ((GAP * F6P) - (X5P * E4P) / p.q7)

    def v8(self, p, DHAP, E4P, SBP):
        """Reversible, fast EQ reaction: DHAP + E4P <-> SBP

        (?) Dihydroxyacetone phosphate + (?) Erythrose 4-phosphate
        -- Aldolase (ALD)-->
        (?) Sedoheptulose 1,7-bisphosphate
        """
        return p.kRE * ((DHAP * E4P) - (SBP) / p.q8)

    def v9(self, p, SBP, P):
        """Irreversible reaction: SBP -> S7P

        (?) Sedoheptulose 1,7-bisphosphate + H20
        --Sedoheptulose 1,7-bisphosphatase (SBPase)-->
        (?) Sedoheptulose 7-phosphate + (?) Pi
        """
        return (p.V9 * SBP) / (SBP + p.Km9 * (1 + (P / p.Ki9)))

    def v10(self, p, GAP, S7P, X5P, R5P):
        """Reversible, fast EQ reaction: S7P + GAP <-> R5P + X5P

        (?) Sedoheptulose 7-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK)-->
        Ribulose 5-phosphate + Xylulose 5-phosphate
        """
        return p.kRE * ((GAP * S7P) - (X5P * R5P) / p.q10)

    def v11(self, p, R5P, RU5P):
        """Reversible, fast EQ reaction: R5P <-> Ru5P

        (?) Ribulose 5-phosphate
        -- Ribose 5-hosphate isomerase (RPI)-->
        (?) Ribulose 5-phosphate
        """
        return p.kRE * ((R5P) - (RU5P) / p.q11)

    def v12(self, p, X5P, RU5P):
        """Reversible, fast EQ reaction: X5P <-> Ru5P

        (?) Xylulose 5-phosphate
        -- Ribulose 5-phosphate 3 epimerase (RPE)-->
        (?) Ribulose 5-phosphate
        """
        return p.kRE * ((X5P) - (RU5P) / p.q12)

    def v13(self, p, RU5P, ATP, PGA, RUBP, P, ADP):
        """Irreversible reaction: Ru5P + ATP -> RuBP + ADP

        (3) Ribulose 5-phosphate + (3) ATP
        -- Phosphoribulokinase (PRK)-->
        (3) Ribulose 1,5-bisphosphate + (3) ADP
        """
        return (p.V13 * RU5P * ATP) / (
            (RU5P + p.Km131 * (1 + (PGA / p.Ki131) + (RUBP / p.Ki132) + (P / p.Ki133)))
            * (ATP * (1 + (ADP / p.Ki134)) + p.Km132 * (1 + (ADP / p.Ki135)))
        )

    def v14(self, p, F6P, G6P):
        """Reversible, fast EQ reaction: F6P <-> G6P
        F6P
        -- Glucose 6-Phosphate isomerase (GPI)-->
        G6P
        """
        return p.kRE * ((F6P) - (G6P) / p.q14)

    def v15(self, p, G6P, G1P):
        """Reversible, fast EQ reaction: G6P <-> G1P
        G6P
        -- Phosphoglucomutase (PGM)
        --> G1P
        """
        return p.kRE * ((G6P) - (G1P) / p.q15)

    def v16(self, p, ADP, Phosphate_i):
        """ATP regeneration  via ATP Synthase"""
        return (p.V16 * ADP * Phosphate_i) / ((ADP + p.Km161) * (Phosphate_i + p.Km162))

    def vPGA_out(self, p, PGA, N):
        """PGA export into medium"""
        return (p.Vx * PGA) / (N * p.Kpga)

    def vGAP_out(self, p, GAP, N):
        """GAP export into medium"""
        return (p.Vx * GAP) / (N * p.Kgap)

    def vDHAP_out(self, p, DHAP, N):
        """DHAP export into medium"""
        return (p.Vx * DHAP) / (N * p.Kdhap)

    def vStarchProduction(self, p, G1P, ATP, ADP, P, PGA, F6P, FBP):
        """G1P -> Gn-1 ; Starch production"""
        return (p.Vst * G1P * ATP) / (
            (G1P + p.Kmst1)
            * (
                (1 + (ADP / p.Kist)) * (ATP + p.Kmst2)
                + ((p.Kmst2 * P) / (p.Kast1 * PGA + p.Kast2 * F6P + p.Kast3 * FBP))
            )
        )


###########################################################################
# Label reactions
###########################################################################
class SplitPoolman:
    ###########################################################################
    # Algebraic
    ###########################################################################

    def ADP(self, p, ATP):
        return [p.Ca - ATP]

    def P_i(
        self,
        p,
        PGA,
        BPGA,
        GAP,
        DHAP,
        FBP,
        F6P,
        G6P,
        G1P,
        SBP,
        S7P,
        E4P,
        X5P,
        R5P,
        RUBP,
        RU5P,
        ATP,
    ):
        return [
            p.Cp
            - (
                PGA
                + 2 * BPGA
                + GAP
                + DHAP
                + 2 * FBP
                + F6P
                + G6P
                + G1P
                + 2 * SBP
                + S7P
                + E4P
                + X5P
                + R5P
                + 2 * RUBP
                + RU5P
                + ATP
            )
        ]

    def N(self, p, P, PGA, GAP, DHAP):
        """Used to calculate vPGA_out, vGAP_out and vDHAP_out"""
        return [
            (
                1
                + (1 + (p.Kpxt / p.Pext))
                * ((P / p.Kpi) + (PGA / p.Kpga) + (GAP / p.Kgap) + (DHAP / p.Kdhap))
            )
        ]

    ###########################################################################
    # Equilibrium
    ###########################################################################

    def v2f(self, p, PGA, ATP):
        """Forward part of
        6 3-Phosphoglycerate + 6 ATP
        -- Phosphoglycerate kinase (PGK) -->
        6 1,3-Bisphosphoglycerate + 6 ADP

        PGA + ATP -> BPGA + ADP
        Assumed to be at equilibrium
        """
        return p.k2f * (ATP * PGA)

    def v2r(self, p, BPGA, ADP):
        """Reverse part of
        6 3-Phosphoglycerate + 6 ATP
        -- Phosphoglycerate kinase (PGK) -->
        6 1,3-Bisphosphoglycerate + 6 ADP

        PGA + ATP -> BPGA + ADP
        Assumed to be at equilibrium
        """
        return p.k2r * (ADP * BPGA) / (p.q2)

    def v3f(self, p, BPGA):
        """Forward part of
        6 1,3-Bisphosphoglycerate + 6 NADPH + 6 H+
        -- Glyceraldehyde 3-phosphate dehydrogenase (GADPH)-->
        6 Glyceraldehyde 3-phosphate (G3P)

        BPGA + NADPH -> GAP + NADP
        Assumed to be at equilibrium
        Stroma pH is assumed to be constant
        """
        return p.k3f * p.NADPH * BPGA * p.protonsStroma

    def v3r(self, p, GAP, P_i):
        """Reverse part of
        6 1,3-Bisphosphoglycerate + 6 NADPH + 6 H+
        -- Glyceraldehyde 3-phosphate dehydrogenase (GADPH)-->
        6 Glyceraldehyde 3-phosphate (G3P)

        BPGA + NADPH -> GAP + NADP
        Assumed to be at equilibrium
        Stroma pH is assumed to be constant
        """
        return p.k3r * GAP * p.NADP * P_i / (p.q3)

    def v4f(self, p, GAP):
        """Forward part of
        (5) Glyceraldehyde 3-phosphate
        -- Triose phosphate isomerae (TPI)-->
        (?) Dihydroxyacetone phosphate

        GAP -> DHAP
        Assumed to be at equilibrium
        """
        return p.k4f * GAP

    def v4r(self, p, DHAP):
        """Reverse part of
        (5) Glyceraldehyde 3-phosphate
        -- Triose phosphate isomerae (TPI)-->
        (?) Dihydroxyacetone phosphate

        GAP -> DHAP
        Assumed to be at equilibrium
        """
        return p.k4r * DHAP / (p.q4)

    def v5f(self, p, DHAP, GAP):
        """Forward part of
        (5) Glyceraldehyde 3-phosphate + (?) Dihydroxyacetone phosphate
        -- Aldolase (ALD)-->
        Frucose 1,6-bisphosphate

        GAP + DHAP <-> FBP
        Assumed to be at equilibrium
        """
        return p.k5f * GAP * DHAP

    def v5r(self, p, FBP):
        """Reverse part of
        (5) Glyceraldehyde 3-phosphate + (?) Dihydroxyacetone phosphate
        -- Aldolase (ALD)-->
        Frucose 1,6-bisphosphate

        GAP + DHAP <-> FBP
        Assumed to be at equilibrium
        """
        return p.k5r * FBP / (p.q5)

    def v7f(self, p, F6P, GAP):
        """
        (?) Fructose 6-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK) -->
        (?) Xylulose 5-phosphate + (?) Erythrose 4-phosphate

        GAP + F6P -> X5P + E4P
        Assumed to be at equilibrium
        """
        return p.k7f * GAP * F6P

    def v7r(self, p, X5P, E4P):
        """Forward part of
        (?) Fructose 6-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK) -->
        (?) Xylulose 5-phosphate + (?) Erythrose 4-phosphate

        GAP + F6P -> X5P + E4P
        Assumed to be at equilibrium
        """
        return p.k7r * X5P * E4P / (p.q7)

    def v8f(self, p, DHAP, E4P):
        """Forward part of
        (?) Dihydroxyacetone phosphate + (?) Erythrose 4-phosphate
        -- Aldolase (ALD)-->
        (?) Sedoheptulose 1,7-bisphosphate

        DHAP + E4P -> SBP
        Assumed to be at equilibrium
        """
        return p.k8f * DHAP * E4P

    def v8r(self, p, SBP):
        """Reverse part of
        (?) Dihydroxyacetone phosphate + (?) Erythrose 4-phosphate
        -- Aldolase (ALD)-->
        (?) Sedoheptulose 1,7-bisphosphate

        DHAP + E4P -> SBP
        Assumed to be at equilibrium
        """
        return p.k8r * SBP / (p.q8)

    def v10f(self, p, S7P, GAP):
        """Forward part of
        (?) Sedoheptulose 7-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK)-->
        Ribulose 5-phosphate + Xylulose 5-phosphate

        S7P + GAP -> R5P + X5P
        Assumed to be at equilibrium
        """
        return p.k10f * GAP * S7P

    def v10r(self, p, X5P, R5P):
        """Reverse part of
        (?) Sedoheptulose 7-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK)-->
        Ribulose 5-phosphate + Xylulose 5-phosphate

        S7P + GAP -> R5P + X5P
        Assumed to be at equilibrium
        """
        return p.k10r * X5P * R5P / (p.q10)

    def v11f(self, p, R5P):
        """Forward part of
        (?) Ribulose 5-phosphate
        -- Ribose 5-hosphate isomerase (RPI)-->
        (?) Ribulose 5-phosphate

        R5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k11f * R5P

    def v11r(self, p, RU5P):
        """Reverse part of
        (?) Ribulose 5-phosphate
        -- Ribose 5-hosphate isomerase (RPI)-->
        (?) Ribulose 5-phosphate

        R5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k11r * RU5P / (p.q11)

    def v12f(self, p, X5P):
        """Forward part of
        (?) Xylulose 5-phosphate
        -- Ribulose 5-phosphate 3 epimerase (RPE)-->
        (?) Ribulose 5-phosphate

        X5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k12f * X5P

    def v12r(self, p, RU5P):
        """Reverse part of
        (?) Xylulose 5-phosphate
        -- Ribulose 5-phosphate 3 epimerase (RPE)-->
        (?) Ribulose 5-phosphate

        X5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k12r * RU5P / (p.q12)

    def v14f(self, p, F6P):
        """Forward part of
        F6P -- Glucose 6-Phosphate isomerase (GPI)--> G6P
        Assumed to be at equilibrium
        """
        return p.k14f * F6P

    def v14r(self, p, G6P):
        """Reverse part of
        F6P -- Glucose 6-Phosphate isomerase (GPI)--> G6P
        Assumed to be at equilibrium
        """
        return p.k14r * G6P / (p.q14)

    def v15f(self, p, G6P):
        """Forward part of
        G6P -- Phosphoglucomutase (PGM) --> G1P
        Assumed to be at equilibrium
        """
        return p.k15f * G6P

    def v15r(self, p, G1P):
        """Reverse part of
        G6P -- Phosphoglucomutase (PGM) --> G1P
        Assumed to be at equilibrium
        """
        return p.k15r * G1P / (p.q15)

    #####################
    # Non-equilibrium
    #####################

    def v1(self, p, RUBP, PGA_tot, FBP_tot, SBP_tot, P, RUBP_tot):
        """
        3 Ribulose-1,5-bisphosphate + 3 CO2
        -- RuBisCO -->
        6 3-Phosphoglycerate

        RuBp + CO2 -> PGA
        """
        return (p.V1 * RUBP) / (
            RUBP_tot
            + p.Km1
            * (
                1
                + (PGA_tot / p.Ki11)
                + (FBP_tot / p.Ki12)
                + (SBP_tot / p.Ki13)
                + (P / p.Ki14)
                + (p.NADPH / p.Ki15)
            )
        )

    def v6(self, p, FBP, F6P_tot, P, FBP_tot):
        """
        (?) Fructose 1,6-bisphosphate + (?) H20
        --Fructose 1,6-bisphosphatase (FBPase) -->
        (6) Fructose 6-phosphate + (Pi)
        FBP -> F6P
        """
        return (p.V6 * FBP) / (FBP_tot + p.Km6 * (1 + (F6P_tot / p.Ki61) + (P / p.Ki62)))

    def v9(self, p, SBP, P, SBP_tot):
        """
        (?) Sedoheptulose 1,7-bisphosphate + H20
        --Sedoheptulose 1,7-bisphosphatase (SBPase)-->
        (?) Sedoheptulose 7-phosphate + (?) Pi

        SBP -> S7P
        """
        return (p.V9 * SBP) / (SBP_tot + p.Km9 * (1 + (P / p.Ki9)))

    def v13(self, p, RU5P, ATP, PGA_tot, RUBP_tot, P, ADP, RU5P_tot):
        """
        (3) Ribulose 5-phosphate + (3) ATP
        -- Phosphoribulokinase (PRK)-->
        (3) Ribulose 1,5-bisphosphate + (3) ADP
        Ru5P + ATP -> RuBP + ADP
        """
        return (p.V13 * RU5P * ATP) / (
            (
                RU5P_tot
                + p.Km131
                * (1 + (PGA_tot / p.Ki131) + (RUBP_tot / p.Ki132) + (P / p.Ki133))
            )
            * (ATP * (1 + (ADP / p.Ki134)) + p.Km132 * (1 + (ADP / p.Ki135)))
        )

    def v16(self, p, ADP, Phosphate_i):
        "ATP regeneration  via ATP Synthase"
        return (p.V16 * ADP * Phosphate_i) / ((ADP + p.Km161) * (Phosphate_i + p.Km162))

    def vPGA_out(self, p, PGA, N):
        """PGA export into medium"""
        return (p.Vx * PGA) / (N * p.Kpga)

    def vGAP_out(self, p, GAP, N):
        """GAP export into medium"""
        return (p.Vx * GAP) / (N * p.Kgap)

    def vDHAP_out(self, p, DHAP, N):
        """DHAP export into medium"""
        return (p.Vx * DHAP) / (N * p.Kdhap)

    def vStarchProduction(self, p, G1P, ATP, ADP, P, PGA_tot, F6P_tot, FBP_tot, G1P_tot):
        """G1P -> Gn-1 ; Starch production"""
        return (p.Vst * G1P * ATP) / (
            (G1P_tot + p.Kmst1)
            * (
                (1 + (ADP / p.Kist)) * (ATP + p.Kmst2)
                + (
                    (p.Kmst2 * P)
                    / (p.Kast1 * PGA_tot + p.Kast2 * F6P_tot + p.Kast3 * FBP_tot)
                )
            )
        )


class Bassham(SplitPoolman):
    ###########################################################################
    # Equilibrium
    ###########################################################################

    def v2f(self, p, PGA, ATP):
        """Forward part of
        6 3-Phosphoglycerate + 6 ATP
        -- Phosphoglycerate kinase (PGK) -->
        6 1,3-Bisphosphoglycerate + 6 ADP

        PGA + ATP -> BPGA + ADP
        Assumed to be at equilibrium
        """
        return p.k2f * (ATP * PGA)

    def v2r(self, p, BPGA, ADP):
        """Reverse part of
        6 3-Phosphoglycerate + 6 ATP
        -- Phosphoglycerate kinase (PGK) -->
        6 1,3-Bisphosphoglycerate + 6 ADP

        PGA + ATP -> BPGA + ADP
        Assumed to be at equilibrium
        """
        return p.k2r * (ADP * BPGA)

    def v3f(self, p, BPGA):
        """Forward part of
        6 1,3-Bisphosphoglycerate + 6 NADPH + 6 H+
        -- Glyceraldehyde 3-phosphate dehydrogenase (GADPH)-->
        6 Glyceraldehyde 3-phosphate (G3P)

        BPGA + NADPH -> GAP + NADP
        Assumed to be at equilibrium
        Stroma pH is assumed to be constant
        """
        return p.k3f * p.NADPH * BPGA * p.protonsStroma

    def v3r(self, p, GAP, P_i):
        """Reverse part of
        6 1,3-Bisphosphoglycerate + 6 NADPH + 6 H+
        -- Glyceraldehyde 3-phosphate dehydrogenase (GADPH)-->
        6 Glyceraldehyde 3-phosphate (G3P)

        BPGA + NADPH -> GAP + NADP
        Assumed to be at equilibrium
        Stroma pH is assumed to be constant
        """
        return p.k3r * GAP * p.NADP * P_i

    def v4f(self, p, GAP):
        """Forward part of
        (5) Glyceraldehyde 3-phosphate
        -- Triose phosphate isomerae (TPI)-->
        (?) Dihydroxyacetone phosphate

        GAP -> DHAP
        Assumed to be at equilibrium
        """
        return p.k4f * GAP

    def v4r(self, p, DHAP):
        """Reverse part of
        (5) Glyceraldehyde 3-phosphate
        -- Triose phosphate isomerae (TPI)-->
        (?) Dihydroxyacetone phosphate

        GAP -> DHAP
        Assumed to be at equilibrium
        """
        return p.k4r * DHAP

    def v5f(self, p, DHAP, GAP):
        """Forward part of
        (5) Glyceraldehyde 3-phosphate + (?) Dihydroxyacetone phosphate
        -- Aldolase (ALD)-->
        Frucose 1,6-bisphosphate

        GAP + DHAP <-> FBP
        Assumed to be at equilibrium
        """
        return p.k5f * GAP * DHAP

    def v5r(self, p, FBP):
        """Reverse part of
        (5) Glyceraldehyde 3-phosphate + (?) Dihydroxyacetone phosphate
        -- Aldolase (ALD)-->
        Frucose 1,6-bisphosphate

        GAP + DHAP <-> FBP
        Assumed to be at equilibrium
        """
        return p.k5r * FBP

    def v7f(self, p, F6P, GAP):
        """
        (?) Fructose 6-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK) -->
        (?) Xylulose 5-phosphate + (?) Erythrose 4-phosphate

        GAP + F6P -> X5P + E4P
        Assumed to be at equilibrium
        """
        return p.k7f * GAP * F6P

    def v7r(self, p, X5P, E4P):
        """Forward part of
        (?) Fructose 6-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK) -->
        (?) Xylulose 5-phosphate + (?) Erythrose 4-phosphate

        GAP + F6P -> X5P + E4P
        Assumed to be at equilibrium
        """
        return p.k7r * X5P * E4P

    def v8f(self, p, DHAP, E4P):
        """Forward part of
        (?) Dihydroxyacetone phosphate + (?) Erythrose 4-phosphate
        -- Aldolase (ALD)-->
        (?) Sedoheptulose 1,7-bisphosphate

        DHAP + E4P -> SBP
        Assumed to be at equilibrium
        """
        return p.k8f * DHAP * E4P

    def v8r(self, p, SBP):
        """Reverse part of
        (?) Dihydroxyacetone phosphate + (?) Erythrose 4-phosphate
        -- Aldolase (ALD)-->
        (?) Sedoheptulose 1,7-bisphosphate

        DHAP + E4P -> SBP
        Assumed to be at equilibrium
        """
        return p.k8r * SBP

    def v10f(self, p, S7P, GAP):
        """Forward part of
        (?) Sedoheptulose 7-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK)-->
        Ribulose 5-phosphate + Xylulose 5-phosphate

        S7P + GAP -> R5P + X5P
        Assumed to be at equilibrium
        """
        return p.k10f * GAP * S7P

    def v10r(self, p, X5P, R5P):
        """Reverse part of
        (?) Sedoheptulose 7-phosphate + (?) Glyceraldehyde 3-phosphate
        -- Transketolase (TK)-->
        Ribulose 5-phosphate + Xylulose 5-phosphate

        S7P + GAP -> R5P + X5P
        Assumed to be at equilibrium
        """
        return p.k10r * X5P * R5P

    def v11f(self, p, R5P):
        """Forward part of
        (?) Ribulose 5-phosphate
        -- Ribose 5-hosphate isomerase (RPI)-->
        (?) Ribulose 5-phosphate

        R5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k11f * R5P

    def v11r(self, p, RU5P):
        """Reverse part of
        (?) Ribulose 5-phosphate
        -- Ribose 5-hosphate isomerase (RPI)-->
        (?) Ribulose 5-phosphate

        R5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k11r * RU5P

    def v12f(self, p, X5P):
        """Forward part of
        (?) Xylulose 5-phosphate
        -- Ribulose 5-phosphate 3 epimerase (RPE)-->
        (?) Ribulose 5-phosphate

        X5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k12f * X5P

    def v12r(self, p, RU5P):
        """Reverse part of
        (?) Xylulose 5-phosphate
        -- Ribulose 5-phosphate 3 epimerase (RPE)-->
        (?) Ribulose 5-phosphate

        X5P -> Ru5P
        Assumed to be at equilibrium
        """
        return p.k12r * RU5P

    def v14f(self, p, F6P):
        """Forward part of
        F6P -- Glucose 6-Phosphate isomerase (GPI)--> G6P
        Assumed to be at equilibrium
        """
        return p.k14f * F6P

    def v14r(self, p, G6P):
        """Reverse part of
        F6P -- Glucose 6-Phosphate isomerase (GPI)--> G6P
        Assumed to be at equilibrium
        """
        return p.k14r * G6P

    def v15f(self, p, G6P):
        """Forward part of
        G6P -- Phosphoglucomutase (PGM) --> G1P
        Assumed to be at equilibrium
        """
        return p.k15f * G6P

    def v15r(self, p, G1P):
        """Reverse part of
        G6P -- Phosphoglucomutase (PGM) --> G1P
        Assumed to be at equilibrium
        """
        return p.k15r * G1P

    def vPGA_out(self, p, PGA, N):
        """PGA export into medium"""
        return (p.Vx_PGA * PGA) / (N * p.Kpga)

    def vGAP_out(self, p, GAP, N):
        """GAP export into medium"""
        return (p.Vx_GAP * GAP) / (N * p.Kgap)

    def vDHAP_out(self, p, DHAP, N):
        """DHAP export into medium"""
        return (p.Vx_DHAP * DHAP) / (N * p.Kdhap)

# Labeled CBB cycle models

A labeled variant of the Poolman CBB cycle model. Developed as my bachelor's thesis which is also included in the repository. The jupyter notebook contains explanations and analyses performed during the thesis.

## Abstract

The Calvin-Benson-Bassham (CBB) cycle is used by many photosynthetic organisms to fix carbon dioxide. Thus it is one of the most important biochemical pathways on earth. Although it was proposed in 1954, the exact structure of the cycle is still under debate. The photosynthetic Gibbs effect, discovered via the asymmetrical incorporation of radioactive $^{14}CO_2$ into hexoses, is widely accepted as being in accord with the proposed CBB reaction scheme, but the given arguments have been mostly qualitatively. In this work a mathematical model is used to provide a quantitative explanation of the
photosynthetic Gibbs effect.

## Models

All models are derived from the Poolman implementation (2000) of the Pettersson CBB model (1988). They are named
* Poolman, for the base model
* LabelDummyPoolman: this is the same model, but the forward and backward reactions are separated, a prerequisite of the label model construction. All compounds are set to have no labelable positions. Used for simulations both with the Poolman, as with the parameters from concentrations and free energies measured by Bassham, which were calculated during this work.
* LabelPoolman: A labeled version of the Poolman model. Used for simulations both with the Poolman and Bassham parameters.

## Parameters

Parameters are stored in a ParameterSet class, which is part of the modelbase_mini construction.

* Poolman: Original parameters
* SplitPoolman: Poolman parameters with separated forward and backwards reactions. Used for the LabelDummyPoolman model
* Bassham: Parameters calculated from concentrations and free energies measured by Bassham 1969

## Reactions

* Poolman: Original Poolman reactions
* SplitPoolman: Poolman reactions with separated forward and backwards reactions. Used for the LabelDummyPoolman model
* Bassham: Modified reactions, due to the new Bassham parameters and removal of the rapid equilibrium constant.


## Custom Packages

The models in this work are created using modelbase_mini, a fork of the [modelbase](https://gitlab.com/ebenhoeh/modelbase) project. The matplotlib styles used are imported with qtb_plot, the standard style sheet of the [QTB Institute](https://www.qtb.hhu.de/).

## Misc

In several analysis the multiprocessing module is used, thus it is recommended to run the notebook on either Linux or Mac, as Windows does not behave well with multiprocessing.
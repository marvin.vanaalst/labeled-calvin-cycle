# Contains:
# Poolman
# LabelDummyPoolman
# LabelPoolman

import modelbase_mini as mbm


class Poolman(mbm.Model):
    def __init__(self, p, r):
        super().__init__(p)
        self.add_compounds(
            [
                "PGA",
                "BPGA",
                "GAP",
                "DHAP",
                "FBP",
                "F6P",
                "E4P",
                "G6P",
                "G1P",
                "SBP",
                "S7P",
                "X5P",
                "R5P",
                "RUBP",
                "RU5P",
                "ATP",
            ]
        )
        self.add_algebraic_module("ADP_module", r.ADP, ["ATP"], ["ADP"])
        self.add_algebraic_module(
            "Phosphate_module",
            r.P_i,
            [
                "PGA",
                "BPGA",
                "GAP",
                "DHAP",
                "FBP",
                "F6P",
                "G6P",
                "G1P",
                "SBP",
                "S7P",
                "E4P",
                "X5P",
                "R5P",
                "RUBP",
                "RU5P",
                "ATP",
            ],
            ["P"],
        )
        self.add_algebraic_module("N_module", r.N, ["P", "PGA", "GAP", "DHAP"], ["N"])
        self.add_reaction(
            "v1", r.v1, {"PGA": 2, "RUBP": -1}, "RUBP", "PGA", "FBP", "SBP", "P"
        )
        self.add_reaction(
            "v2", r.v2, {"PGA": -1, "ATP": -1, "BPGA": 1}, "ATP", "PGA", "ADP", "BPGA"
        )
        self.add_reaction("v3", r.v3, {"BPGA": -1, "GAP": 1}, "BPGA", "GAP", "P")
        self.add_reaction("v4", r.v4, {"GAP": -1, "DHAP": 1}, "GAP", "DHAP")
        self.add_reaction(
            "v5", r.v5, {"GAP": -1, "DHAP": -1, "FBP": 1}, "GAP", "DHAP", "FBP"
        )
        self.add_reaction("v6", r.v6, {"FBP": -1, "F6P": 1}, "FBP", "F6P", "P")
        self.add_reaction(
            "v7",
            r.v7,
            {"GAP": -1, "F6P": -1, "E4P": 1, "X5P": 1},
            "GAP",
            "F6P",
            "X5P",
            "E4P",
        )
        self.add_reaction(
            "v8", r.v8, {"DHAP": -1, "E4P": -1, "SBP": 1}, "DHAP", "E4P", "SBP"
        )
        self.add_reaction("v9", r.v9, {"SBP": -1, "S7P": 1}, "SBP", "P")
        self.add_reaction(
            "v10",
            r.v10,
            {"GAP": -1, "S7P": -1, "X5P": 1, "R5P": 1},
            "GAP",
            "S7P",
            "X5P",
            "R5P",
        )
        self.add_reaction("v11", r.v11, {"R5P": -1, "RU5P": 1}, "R5P", "RU5P")
        self.add_reaction("v12", r.v12, {"X5P": -1, "RU5P": 1}, "X5P", "RU5P")
        self.add_reaction(
            "v13",
            r.v13,
            {"ATP": -1, "RU5P": -1, "RUBP": 1},
            "RU5P",
            "ATP",
            "PGA",
            "RUBP",
            "P",
            "ADP",
        )
        self.add_reaction("v14", r.v14, {"F6P": -1, "G6P": 1}, "F6P", "G6P")
        self.add_reaction("v15", r.v15, {"G6P": -1, "G1P": 1}, "G6P", "G1P")
        self.add_reaction("v16", r.v16, {"ATP": 1}, "ADP", "P")
        self.add_reaction("vPGA_out", r.vPGA_out, {"PGA": -1}, "PGA", "N")
        self.add_reaction("vGAP_out", r.vGAP_out, {"GAP": -1}, "GAP", "N")
        self.add_reaction("vDHAP_out", r.vDHAP_out, {"DHAP": -1}, "DHAP", "N")
        self.add_reaction(
            "vSt",
            r.vStarchProduction,
            {"G1P": -1, "ATP": -1},
            "G1P",
            "ATP",
            "ADP",
            "P",
            "PGA",
            "F6P",
            "FBP",
        )


# Dummy label models without actual labeling
class LabelDummyPoolman(mbm.LabelModel):
    def __init__(self, p, r):
        super().__init__(p)

        carbon_compounds = {
            "ATP": 0,
            "PGA": 0,
            "BPGA": 0,
            "GAP": 0,
            "DHAP": 0,
            "FBP": 0,
            "F6P": 0,
            "E4P": 0,
            "G6P": 0,
            "G1P": 0,
            "SBP": 0,
            "S7P": 0,
            "X5P": 0,
            "R5P": 0,
            "RU5P": 0,
            "RUBP": 0,
        }

        self.add_carbon_compounds(carbon_compounds)
        self.add_algebraic_module("ADP_module", r.ADP, ["ATP"], ["ADP"])
        self.add_algebraic_module(
            "Phosphate_module",
            r.P_i,
            [
                "PGA",
                "BPGA",
                "GAP",
                "DHAP",
                "FBP",
                "F6P",
                "G6P",
                "G1P",
                "SBP",
                "S7P",
                "E4P",
                "X5P",
                "R5P",
                "RUBP",
                "RU5P",
                "ATP",
            ],
            ["P"],
        )
        self.add_algebraic_module("N_module", r.N, ["P", "PGA", "GAP", "DHAP"], ["N"])
        self.add_carbonmap_reaction(
            "v2f", r.v2f, [], ["PGA", "ATP"], ["BPGA"], "PGA", "ATP"
        )
        self.add_carbonmap_reaction(
            "v2r", r.v2r, [], ["BPGA"], ["PGA", "ATP"], "BPGA", "ADP"
        )
        self.add_carbonmap_reaction("v3f", r.v3f, [], ["BPGA"], ["GAP"], "BPGA")
        self.add_carbonmap_reaction("v3r", r.v3r, [], ["GAP"], ["BPGA"], "GAP", "P")
        self.add_carbonmap_reaction("v4f", r.v4f, [], ["GAP"], ["DHAP"], "GAP")
        self.add_carbonmap_reaction("v4r", r.v4r, [], ["DHAP"], ["GAP"], "DHAP")
        self.add_carbonmap_reaction(
            "v5f", r.v5f, [], ["DHAP", "GAP"], ["FBP"], "DHAP", "GAP"
        )
        self.add_carbonmap_reaction("v5r", r.v5r, [], ["FBP"], ["DHAP", "GAP"], "FBP")
        self.add_carbonmap_reaction(
            "v7f", r.v7f, [], ["F6P", "GAP"], ["X5P", "E4P"], "F6P", "GAP"
        )
        self.add_carbonmap_reaction(
            "v7r", r.v7r, [], ["X5P", "E4P"], ["F6P", "GAP"], "X5P", "E4P"
        )
        self.add_carbonmap_reaction(
            "v8f", r.v8f, [], ["DHAP", "E4P"], ["SBP"], "DHAP", "E4P"
        )
        self.add_carbonmap_reaction("v8r", r.v8r, [], ["SBP"], ["DHAP", "E4P"], "SBP")
        self.add_carbonmap_reaction(
            "v10f", r.v10f, [], ["S7P", "GAP"], ["X5P", "R5P"], "S7P", "GAP"
        )
        self.add_carbonmap_reaction(
            "v10r", r.v10r, [], ["X5P", "R5P"], ["S7P", "GAP"], "X5P", "R5P"
        )
        self.add_carbonmap_reaction("v11f", r.v11f, [], ["R5P"], ["RU5P"], "R5P")
        self.add_carbonmap_reaction("v11r", r.v11r, [], ["RU5P"], ["R5P"], "RU5P")
        self.add_carbonmap_reaction("v12f", r.v12f, [], ["X5P"], ["RU5P"], "X5P")
        self.add_carbonmap_reaction("v12r", r.v12r, [], ["RU5P"], ["X5P"], "RU5P")
        self.add_carbonmap_reaction("v14f", r.v14f, [], ["F6P"], ["G6P"], "F6P")
        self.add_carbonmap_reaction("v14r", r.v14r, [], ["G6P"], ["F6P"], "G6P")
        self.add_carbonmap_reaction("v15f", r.v15f, [], ["G6P"], ["G1P"], "G6P")
        self.add_carbonmap_reaction("v15r", r.v15r, [], ["G1P"], ["G6P"], "G1P")
        self.add_carbonmap_reaction(
            "v1",
            r.v1,
            [],
            ["RUBP"],
            ["PGA", "PGA"],
            "RUBP",
            "PGA",
            "FBP",
            "SBP",
            "P",
            "RUBP",
        )
        self.add_carbonmap_reaction(
            "v6", r.v6, [], ["FBP"], ["F6P"], "FBP", "F6P", "P", "FBP"
        )
        self.add_carbonmap_reaction("v9", r.v9, [], ["SBP"], ["S7P"], "SBP", "P", "SBP")
        self.add_carbonmap_reaction(
            "v13",
            r.v13,
            [],
            ["RU5P", "ATP"],
            ["RUBP"],
            "RU5P",
            "ATP",
            "PGA",
            "RUBP",
            "P",
            "ADP",
            "RU5P",
        )
        self.add_carbonmap_reaction(
            "vSt",
            r.vStarchProduction,
            [],
            ["G1P", "ATP"],
            [],
            "G1P",
            "ATP",
            "ADP",
            "P",
            "PGA",
            "F6P",
            "FBP",
            "G1P",
        )
        self.add_carbonmap_reaction("vPGA_out", r.vPGA_out, [], ["PGA"], [], "PGA", "N")
        self.add_carbonmap_reaction("vGAP_out", r.vGAP_out, [], ["GAP"], [], "GAP", "N")
        self.add_carbonmap_reaction(
            "vDHAP_out", r.vDHAP_out, [], ["DHAP"], [], "DHAP", "N"
        )
        self.add_reaction("v16", r.v16, {"ATP": 1}, "ADP", "P")


class LabelPoolman(mbm.LabelModel):
    def __init__(self, p, r):
        super().__init__(p)

        carbon_compounds = {
            "ATP": 0,
            "PGA": 3,
            "BPGA": 3,
            "GAP": 3,
            "DHAP": 3,
            "FBP": 6,
            "F6P": 6,
            "E4P": 4,
            "G6P": 6,
            "G1P": 6,
            "SBP": 7,
            "S7P": 7,
            "X5P": 5,
            "R5P": 5,
            "RU5P": 5,
            "RUBP": 5,
        }

        self.add_carbon_compounds(carbon_compounds)
        self.add_algebraic_module("ADP_module", r.ADP, ["ATP"], ["ADP"])

        self.add_algebraic_module(
            "Phosphate_module",
            r.P_i,
            [
                "PGA_total",
                "BPGA_total",
                "GAP_total",
                "DHAP_total",
                "FBP_total",
                "F6P_total",
                "G6P_total",
                "G1P_total",
                "SBP_total",
                "S7P_total",
                "E4P_total",
                "X5P_total",
                "R5P_total",
                "RUBP_total",
                "RU5P_total",
                "ATP",
            ],
            ["P"],
        )
        self.add_algebraic_module(
            "N_module", r.N, ["P", "PGA_total", "GAP_total", "DHAP_total"], ["N"]
        )
        self.add_carbonmap_reaction(
            "v2f", r.v2f, [0, 1, 2], ["PGA", "ATP"], ["BPGA"], "PGA", "ATP"
        )
        self.add_carbonmap_reaction(
            "v2r", r.v2r, [0, 1, 2], ["BPGA"], ["PGA", "ATP"], "BPGA", "ADP"
        )
        self.add_carbonmap_reaction("v3f", r.v3f, [0, 1, 2], ["BPGA"], ["GAP"], "BPGA")
        self.add_carbonmap_reaction(
            "v3r", r.v3r, [0, 1, 2], ["GAP"], ["BPGA"], "GAP", "P"
        )
        self.add_carbonmap_reaction("v4f", r.v4f, [2, 1, 0], ["GAP"], ["DHAP"], "GAP")
        self.add_carbonmap_reaction("v4r", r.v4r, [2, 1, 0], ["DHAP"], ["GAP"], "DHAP")
        self.add_carbonmap_reaction(
            "v5f", r.v5f, [0, 1, 2, 3, 4, 5], ["DHAP", "GAP"], ["FBP"], "DHAP", "GAP"
        )
        self.add_carbonmap_reaction(
            "v5r",
            r.v5r,
            [
                0,
                1,
                2,
                3,
                4,
                5,
            ],
            ["FBP"],
            ["DHAP", "GAP"],
            "FBP",
        )
        self.add_carbonmap_reaction(
            "v7f",
            r.v7f,
            [0, 1, 6, 7, 8, 2, 3, 4, 5],
            ["F6P", "GAP"],
            ["X5P", "E4P"],
            "F6P",
            "GAP",
        )
        self.add_carbonmap_reaction(
            "v7r",
            r.v7r,
            [0, 1, 5, 6, 7, 8, 2, 3, 4],
            ["X5P", "E4P"],
            ["F6P", "GAP"],
            "X5P",
            "E4P",
        )
        self.add_carbonmap_reaction(
            "v8f", r.v8f, [0, 1, 2, 3, 4, 5, 6], ["DHAP", "E4P"], ["SBP"], "DHAP", "E4P"
        )
        self.add_carbonmap_reaction(
            "v8r", r.v8r, [0, 1, 2, 3, 4, 5, 6], ["SBP"], ["DHAP", "E4P"], "SBP"
        )
        self.add_carbonmap_reaction(
            "v10f",
            r.v10f,
            [0, 1, 7, 8, 9, 2, 3, 4, 5, 6],
            ["S7P", "GAP"],
            ["X5P", "R5P"],
            "S7P",
            "GAP",
        )
        self.add_carbonmap_reaction(
            "v10r",
            r.v10r,
            [0, 1, 5, 6, 7, 8, 9, 2, 3, 4],
            ["X5P", "R5P"],
            ["S7P", "GAP"],
            "X5P",
            "R5P",
        )
        self.add_carbonmap_reaction(
            "v11f", r.v11f, [0, 1, 2, 3, 4], ["R5P"], ["RU5P"], "R5P"
        )
        self.add_carbonmap_reaction(
            "v11r", r.v11r, [0, 1, 2, 3, 4], ["RU5P"], ["R5P"], "RU5P"
        )
        self.add_carbonmap_reaction(
            "v12f", r.v12f, [0, 1, 2, 3, 4], ["X5P"], ["RU5P"], "X5P"
        )
        self.add_carbonmap_reaction(
            "v12r", r.v12r, [0, 1, 2, 3, 4], ["RU5P"], ["X5P"], "RU5P"
        )
        self.add_carbonmap_reaction(
            "v14f", r.v14f, [0, 1, 2, 3, 4, 5], ["F6P"], ["G6P"], "F6P"
        )
        self.add_carbonmap_reaction(
            "v14r", r.v14r, [0, 1, 2, 3, 4, 5], ["G6P"], ["F6P"], "G6P"
        )
        self.add_carbonmap_reaction(
            "v15f", r.v15f, [0, 1, 2, 3, 4, 5], ["G6P"], ["G1P"], "G6P"
        )
        self.add_carbonmap_reaction(
            "v15r", r.v15r, [0, 1, 2, 3, 4, 5], ["G1P"], ["G6P"], "G1P"
        )
        self.add_carbonmap_reaction(
            "v1",
            r.v1,
            [2, 1, 0, 5, 3, 4],
            ["RUBP"],
            ["PGA", "PGA"],
            "RUBP",
            "PGA_total",
            "FBP_total",
            "SBP_total",
            "P",
            "RUBP_total",
        )
        self.add_carbonmap_reaction(
            "v6",
            r.v6,
            [0, 1, 2, 3, 4, 5],
            ["FBP"],
            ["F6P"],
            "FBP",
            "F6P_total",
            "P",
            "FBP_total",
        )
        self.add_carbonmap_reaction(
            "v9", r.v9, [0, 1, 2, 3, 4, 5, 6], ["SBP"], ["S7P"], "SBP", "P", "SBP_total"
        )
        self.add_carbonmap_reaction(
            "v13",
            r.v13,
            [0, 1, 2, 3, 4],
            ["RU5P", "ATP"],
            ["RUBP"],
            "RU5P",
            "ATP",
            "PGA_total",
            "RUBP_total",
            "P",
            "ADP",
            "RU5P_total",
        )
        self.add_carbonmap_reaction(
            "vSt",
            r.vStarchProduction,
            [0, 1, 2, 3, 4, 5],
            ["G1P", "ATP"],
            [],
            "G1P",
            "ATP",
            "ADP",
            "P",
            "PGA_total",
            "F6P_total",
            "FBP_total",
            "G1P_total",
        )
        self.add_carbonmap_reaction(
            "vPGA_out", r.vPGA_out, [0, 1, 2], ["PGA"], [], "PGA", "N"
        )
        self.add_carbonmap_reaction(
            "vGAP_out", r.vGAP_out, [0, 1, 2], ["GAP"], [], "GAP", "N"
        )
        self.add_carbonmap_reaction(
            "vDHAP_out", r.vDHAP_out, [0, 1, 2], ["DHAP"], [], "DHAP", "N"
        )
        self.add_reaction("v16", r.v16, {"ATP": 1}, "ADP", "P")
